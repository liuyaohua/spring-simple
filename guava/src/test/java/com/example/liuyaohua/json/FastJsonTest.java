package com.example.liuyaohua.json;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.annotation.JSONField;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * <p>作者 yaohua.liu
 * <p>日期 2018-11-15 15:55
 * <p>说明 ...
 */
public class FastJsonTest {
    PeopleTest p;

    @Before
    public void init() {
        p = new PeopleTest();
        p.setAge(20);
        p.setName("People");
        p.setSetName(true);
        p.setAddressName("xxx");
    }

    @Test
    public void test() {
        String json = JSON.toJSONString(p);
        System.out.println("生成json串："+json);

        PeopleTest newPeople = JSON.parseObject(json, PeopleTest.class);
        System.out.println("解析成对象："+JSON.toJSONString(newPeople));

        Assert.assertNotNull(newPeople);
        Assert.assertEquals(newPeople.getAge(),p.getAge());
        Assert.assertEquals(newPeople.getSetName(),p.getSetName());
        Assert.assertEquals(newPeople.getAddressName(),p.getAddressName());
    }
}

class PeopleTest {
    @JSONField(serialize = false, deserialize = false)
    String name;
    @JSONField(name = "aaa")
    int age;
    boolean setName = false;
    @JSONField(name = "address_name")
    String addressName = "";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean getSetName() {
        return setName;
    }

    public void setSetName(boolean setName) {
        this.setName = setName;
    }

    public String getAddressName() {
        return addressName;
    }

    public void setAddressName(String addressName) {
        this.addressName = addressName;
    }

    @Override
    public String toString() {
        return "People{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", setName=" + setName +
                ", addressName='" + addressName + '\'' +
                '}';
    }
}