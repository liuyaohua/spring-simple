package com.example.liuyaohua.guava.base;

import com.google.common.base.Objects;
import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;
import org.junit.Test;

/**
 * 作者 yaohua.liu
 * 日期 2014-10-20 11:41
 *
 * 说明 ...
 */
public class ObjectsTest {

    @Test
    public void testObjects() {
        // 比较两个对象是否相等，会比较两个对象的hashcode
        Objects.equal("a", "b");

        // 获取所有对象的hashCode值
        int code = Objects.hashCode("a", "b","c");
        int code1=Objects.hashCode("a","c","b");

        //适合用于对象重写toString()
        int x=10;
        String str="xxxx";
        String result = Objects.toStringHelper(this)
                .add("x",x)
                .add("str",str)
                .toString();

        // 第一个不为空返回第一个,第一个为空时返回第二个,两个都为空时抛出异常
        Object s1 = Objects.firstNonNull("a", "b");
        Object s2 = Objects.firstNonNull(null, "b");
        //Objects.firstNonNull(null, null);//异常
    }

    /**
     * 返回本身||引用的hashCode比较值
     * <p/>
     * 其它：
     * 1、== ： 对象之间的比较都是比较其内存地址
     * 2、equal：比较其hashCode
     */
    @Test
    public void testEqual() {

        Integer a1 = 0, b1 = 5;
        System.out.println("-1 : " + (a1 == b1 ? "a1 == b1" : "a1 !=b1 "));//a==b ； a b内存地址不同

        Object a = "aaa";
        Object b = "aaa";
        System.out.println("0 : " + (a == b ? "a == b" : "a !=b "));//a==b ； a b内存地址相同

        Preconditions.checkState(Objects.equal(a, b), "1 : %s != %s", a, b);//true ； hashCode相同 且 a b内存地址相同

        a = new String("aaa");
        b = new String("aaa");
        Preconditions.checkState(Objects.equal(a, b), "2 : %s != %s", a, b);//true ; hashCode相同

        System.out.println("3 : " + (a == b ? "a == b" : "a !=b "));//a != b ；内存地址不一样

/*        Map<String,Object>m1= Maps.newHashMap().put("a","a");
        m1.put("a","a");
        Map<String,Object>m2= Maps.newHashMap();
        m2.put("a","a");
        System.out.println("m1:"+m1.toString()+",hashCode:"+m1.hashCode()+";m2:"+m2.toString()+",hashCode:"+m2.hashCode());*/
        System.out.println(Objects.equal(Maps.newHashMap().put("a", "a"), Maps.newHashMap().put("a", "a")) ? "==" : "!=");
    }

}