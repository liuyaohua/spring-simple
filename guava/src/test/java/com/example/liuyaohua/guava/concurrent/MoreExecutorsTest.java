package com.example.liuyaohua.guava.concurrent;

import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.ListeningScheduledExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import org.junit.Test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 作者 yaohua.liu
 * 日期 2014-11-27 16:34
 * * 说明 ...
 */
public class MoreExecutorsTest {

    @Test
    public void testMoreExecutors(){

        ExecutorService executorService=Executors.newSingleThreadExecutor();

        ListeningExecutorService listeningExecutorService = MoreExecutors.listeningDecorator( Executors.newFixedThreadPool(2));

        ListeningScheduledExecutorService listeningScheduledExecutorService = MoreExecutors.listeningDecorator(Executors.newScheduledThreadPool(2));

        ListeningExecutorService listeningExecutorService1=MoreExecutors.sameThreadExecutor();

    }
}
