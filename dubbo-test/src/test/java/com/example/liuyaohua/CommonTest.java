package com.example.liuyaohua;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * 作者 yaohua.liu
 * 日期 2017-08-24 15:31
 * 说明 ...
 */
@RunWith(SpringJUnit4ClassRunner.class)  //使用junit4进行测试
@ContextConfiguration({"classpath:spring/spring-config.xml"}) //加载配置文件
public class CommonTest {

    @Test
    public void test(){

    }
}
