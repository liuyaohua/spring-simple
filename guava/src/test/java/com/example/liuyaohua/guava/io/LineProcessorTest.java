package com.example.liuyaohua.guava.io;

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import com.google.common.io.LineProcessor;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

/**
 * 作者 yaohua.liu
 * 日期 2014-11-24 15:04
 * * 说明 使用行处理器处理文件读取
 */
public class LineProcessorTest {

    @Test
    public void testFiles() throws IOException {

        File from = new File(this.getClass().getClassLoader().getResource("from.txt").getPath() );

        String result = Files.readLines(from, Charsets.UTF_8, new LineProcessor<String>() {
            StringBuilder stringBuilder = new StringBuilder();
            @Override
            public boolean processLine(String s) throws IOException {
                if (s.contains("System")) {
                    return false;//结束执行
                }
                stringBuilder.append(s + "\n");
                return true;
            }

            @Override
            public String getResult() {// 整个处理器返回结果
                return stringBuilder.toString();
            }
        });
        System.out.println(result);
    }
}
