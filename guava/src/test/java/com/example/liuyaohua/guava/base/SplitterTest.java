package com.example.liuyaohua.guava.base;

import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import org.junit.Test;

import java.util.List;
import java.util.Map;

/**
 * 作者 yaohua.liu
 * 日期 2014-10-17 18:45
 *
 * 说明 ...
 */
public class SplitterTest {

    /**
     * 根据字符分割
     */
    @Test
    public void testSplitter() {
        String params = " foo,,baro, ,";

        Splitter splitter = Splitter.on(',')//支持字符与字符串分割
                //.onPattern("o,")//支持正则表达式
                .trimResults()//去掉空格
                .omitEmptyStrings();//省略空字符串

        Iterable<String> iterable = splitter.split(params);

        // 把iterable转换成列表
        List<String> result= Lists.newArrayList(iterable);
    }

    /**
     * 分割字符串到Map中
     */
    @Test
    public void testSplitterToMap() {
        String params = "a=>b ; c=>d";

        Splitter.MapSplitter mapSplitter = Splitter.on(";")
                .omitEmptyStrings()// 去除结果集中的空串
                .trimResults()// 去除前后端空格
                .withKeyValueSeparator("=>");//key与value的分割符

        Map<String, String > map=mapSplitter.split(params);// 保存结果集到Map中
        String str=map.toString();
    }

    /**
     * 根据正则表达式分割
     */
    @Test
    public void testSplitterOnPattern() {
        String params = " foo,，baro,ccc，ddd ,";
        // ','或'，'
        Splitter splitter = Splitter.onPattern("，|,")
                .omitEmptyStrings()//去除结果集中的空串
                .trimResults();//去除前后端空格

        Iterable<String> it = splitter.split(params);

        List<String> result= Lists.newArrayList(it);

        String join= Joiner.on(",").join(result);
        System.out.println(join);
    }
}
