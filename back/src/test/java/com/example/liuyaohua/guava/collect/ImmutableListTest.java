package com.example.liuyaohua.guava.collect;

import com.google.common.collect.ImmutableList;
import org.junit.Test;

import java.util.Iterator;

/**
 * 作者 yaohua.liu
 * 日期 2014-11-24 17:24
 * * 说明 ...
 */
public class ImmutableListTest {
    @Test
    public void testImmutableList() {
        ImmutableList<String> list = ImmutableList.of("1", "3", "2");
        ImmutableList<String> subList = list.subList(1, 2);
        ImmutableList<String> reverse = list.reverse();

        Iterator<String> iterator = list.iterator();
    }
}
