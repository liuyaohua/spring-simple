package com.example.liuyaohua.cache.lruk;


import com.example.liuyaohua.cache.lruk.lru.status.CacheStats;
import com.example.liuyaohua.cache.lruk.lru.status.CacheStats;
import com.example.liuyaohua.cache.lruk.sync.EnumLrukSyncType;

/**
 * 作者 yaohua.liu
 * 日期 2018-04-03 11:37
 * 说明 lruk本地缓存
 * <p>
 * k结构：
 * 1024(行) x 2048(列) = 200w
 * 阀值：2(小于2时不从本地lruk缓存中取数据)
 * 命中率日志输出开关：开
 * lru本地缓存：最大量1024
 * 本地缓存时间：1分种 =  60
 */
public class LrukSimple extends AbstractLrukCache<Integer, MutableInteger> implements LrukCache<Integer, MutableInteger> {
    public LrukSimple() {

    }

    public LrukSimple(int k) {
        this.k = k;
    }

    // 可从阿波罗动态获取值
    private volatile int k = 1;

    // 测试用
    public void setK(int k) {
        this.k = k;
    }

    private MutableInteger mutableInteger = new MutableInteger(10000, 10000);

    @Override
    public MutableInteger load(Integer key) {
        return mutableInteger;
    }

    @Override
    public MutableInteger get(Integer key) throws Exception {
        return super.get(key);
    }

    @Override
    public MutableInteger getUnchecked(Integer key) {
        return super.getUnchecked(key);
    }

    @Override
    public int getK() {
        return this.k;
    }

    @Override
    public long initMaximumSize() {
        return 1024;
    }

    @Override
    public long initTimeOut() {
        return 60;
    }

    /**
     * 获取K命中率开关，如果开关打开则日志输出命中率信息
     *
     * @return
     */
    public boolean initKHitRateSwitch() {
        return true;
    }

    /**
     * 初始化K结构行大小
     */
    protected int initKRowSize() {
        return 1024;
    }

    /**
     * 初始化K结构列大小
     */
    protected int initKColumnSize() {
        return 2048;
    }

    @Override
    protected String getSyncAddressStr() {
        return "";
    }

    @Override
    protected EnumLrukSyncType getSyncType() {
        return EnumLrukSyncType.HOUSE;
    }

    /**
     * k阀值命中率状态统计，如果最后一次记录时间超时则重置该时间
     *
     * @return CacheStats
     */
    public CacheStats kHitRateStats() {
        return super.kHitRateStats();
    }
}
