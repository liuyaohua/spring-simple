package com.example.liuyaohua.designpattern;

/**
 * 作者： yaohua.liu
 * 日期： 2017-06-28
 * 描述： 解释器模式是我们暂时的最后一讲，一般主要应用在OOP开发中的编译器的开发中，所以适用面比较窄。
 */
public class D23Interpreter {

	public static void main(String[] args) {
		// 计算9+2-8的值
		int result = new MinusT().interpret((new ContextT(new PlusT()
				.interpret(new ContextT(9, 2)), 8)));
		System.out.println(result);
	}
}

interface Expression {
	public int interpret(ContextT context);
}

class PlusT implements Expression {

	@Override
	public int interpret(ContextT context) {
		return context.getNum1() + context.getNum2();
	}
}

class MinusT implements Expression {

	@Override
	public int interpret(ContextT context) {
		return context.getNum1() - context.getNum2();
	}
}

class ContextT {

	private int num1;
	private int num2;

	public ContextT(int num1, int num2) {
		this.num1 = num1;
		this.num2 = num2;
	}

	public int getNum1() {
		return num1;
	}

	public void setNum1(int num1) {
		this.num1 = num1;
	}

	public int getNum2() {
		return num2;
	}

	public void setNum2(int num2) {
		this.num2 = num2;
	}
}

