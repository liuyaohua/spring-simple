package com.example.liuyaohua.model.enums;

/**
 * 作者 yaohua.liu
 * 日期 2016-08-08 16:56
 * 说明 渠道类型
 */
public enum ChannelType {
    FUND("公积金", "properties/fund"),
    KANIU("卡牛", "properties/kn"),
    HTML5("html5", "properties/html5"),
    WA_CAI("挖财", "properties/wacai"),
    XYGJ("信用管家", "properties/xygj"),
    COMMON("通用渠道", "properties/common"),
    UTEST("测试", "properties/utest")
    ;

    /**
     * 渠道名称
     */
    private String name;
    /**
     * 渠道的配置文件路径，都在properties目录中
     */
    private String propertiesFilePath;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPropertiesFilePath() {
        return propertiesFilePath;
    }

    public void setPropertiesFilePath(String propertiesFilePath) {
        this.propertiesFilePath = propertiesFilePath;
    }

    ChannelType(String name, String propertiesFilePath) {
        this.name = name;
        this.propertiesFilePath = propertiesFilePath;
    }

}
