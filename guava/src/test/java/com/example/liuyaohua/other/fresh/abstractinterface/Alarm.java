package com.example.liuyaohua.other.fresh.abstractinterface;

/**
 * 作者 yaohua.liu
 * 日期 2014-11-21 19:08
 * 包名 com.qunar.fresh.abstractinterface
 * 说明 ...
 */
public interface Alarm {

    void alarm();

}
