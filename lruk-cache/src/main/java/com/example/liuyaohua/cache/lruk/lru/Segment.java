package com.example.liuyaohua.cache.lruk.lru;

import com.example.liuyaohua.cache.lruk.lru.entry.ReferenceEntry;
import com.example.liuyaohua.cache.lruk.lru.queue.AccessQueue;
import com.example.liuyaohua.cache.lruk.lru.queue.WriteQueue;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.util.concurrent.*;
import com.example.liuyaohua.cache.lruk.lru.entry.ReferenceEntry;
import com.example.liuyaohua.cache.lruk.lru.queue.AccessQueue;
import com.example.liuyaohua.cache.lruk.lru.queue.WriteQueue;
import com.example.liuyaohua.cache.lruk.lru.reference.LoadingValueReference;
import com.example.liuyaohua.cache.lruk.lru.reference.ValueReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReferenceArray;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.BiFunction;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import static com.google.common.util.concurrent.MoreExecutors.directExecutor;
import static com.google.common.util.concurrent.Uninterruptibles.getUninterruptibly;

/**
 * Segments are specialized versions of hash tables. This subclass inherits from ReentrantLock
 * opportunistically, just to simplify some locking and avoid separate construction.
 */
public class Segment<K, V> extends ReentrantLock {

    private static final Logger logger = LoggerFactory.getLogger(Segment.class.getName());

    final LocalCache<K, V> map;

    /**
     * The number of live elements in this segment's region.
     */
    public volatile int count;

    /**
     * The weight of the live elements in this segment's region.
     */

    public long totalWeight;

    /**
     * Number of updates that alter the size of the table. This is used during bulk-read methods to
     * make sure they see a consistent snapshot: If modCounts change during a traversal of segments
     * loading size or checking containsValue, then we might have an inconsistent view of state so
     * (usually) must retry.
     */
    public int modCount;

    /**
     * The table is expanded when its size exceeds this threshold. (The value of this field is
     * always {@code (int) (capacity * 0.75)}.)
     */
    public int threshold;

    /**
     * The per-segment table.
     */
    public volatile AtomicReferenceArray<ReferenceEntry<K, V>> table;

    /**
     * The maximum weight of this segment. UNSET_INT if there is no maximum.
     */
    public final long maxSegmentWeight;

    /**
     * The key reference queue contains entries whose keys have been garbage collected, and which
     * need to be cleaned up internally.
     */
    public final ReferenceQueue<K> keyReferenceQueue;

    /**
     * The value reference queue contains value references whose values have been garbage collected,
     * and which need to be cleaned up internally.
     */
    public final ReferenceQueue<V> valueReferenceQueue;

    /**
     * The recency queue is used to record which entries were accessed for updating the access
     * list's ordering. It is drained as a batch operation when either the DRAIN_THRESHOLD is
     * crossed or a write occurs on the segment.
     */
    public final Queue<ReferenceEntry<K, V>> recencyQueue;

    /**
     * A counter of the number of reads since the last write, used to drain queues on a small
     * fraction of read operations.
     */
    public final AtomicInteger readCount = new AtomicInteger();

    /**
     * A queue of elements currently in the map, ordered by write time. Elements are added to the
     * tail of the queue on write.
     */

    public final Queue<ReferenceEntry<K, V>> writeQueue;

    /**
     * A queue of elements currently in the map, ordered by access time. Elements are added to the
     * tail of the queue on access (note that writes count as accesses).
     */

    public final Queue<ReferenceEntry<K, V>> accessQueue;

    /**
     * Accumulates lruk statistics.
     */
    public final StatsCounter statsCounter;

    public Segment(
            LocalCache<K, V> map,
            int initialCapacity,
            long maxSegmentWeight,
            StatsCounter statsCounter) {
        this.map = map;
        this.maxSegmentWeight = maxSegmentWeight;
        this.statsCounter = checkNotNull(statsCounter);
        initTable(newEntryArray(initialCapacity));

        keyReferenceQueue = map.usesKeyReferences() ? new ReferenceQueue<K>() : null;

        valueReferenceQueue = map.usesValueReferences() ? new ReferenceQueue<V>() : null;

        recencyQueue = map.usesAccessQueue() ? new ConcurrentLinkedQueue<ReferenceEntry<K, V>>() : LocalCache.<ReferenceEntry<K, V>>discardingQueue();

        writeQueue = map.usesWriteQueue() ? new WriteQueue<K, V>() : LocalCache.<ReferenceEntry<K, V>>discardingQueue();

        accessQueue = map.usesAccessQueue() ? new AccessQueue<K, V>() : LocalCache.<ReferenceEntry<K, V>>discardingQueue();
    }

    AtomicReferenceArray<ReferenceEntry<K, V>> newEntryArray(int size) {
        return new AtomicReferenceArray<>(size);
    }

    void initTable(AtomicReferenceArray<ReferenceEntry<K, V>> newTable) {
        this.threshold = newTable.length() * 3 / 4; // 0.75
        if (!map.customWeigher() && this.threshold == maxSegmentWeight) {
            // prevent spurious expansion before eviction
            this.threshold++;
        }
        this.table = newTable;
    }

    public ReferenceEntry<K, V> newEntry(K key, int hash, ReferenceEntry<K, V> next) {
        return map.entryFactory.newEntry(this, checkNotNull(key), hash, next);
    }

    /**
     * Copies {@code original} into a new entry chained to {@code newNext}.
     * Returns the new entry, or {@code null} if {@code original} was already garbage collected.
     */
    public ReferenceEntry<K, V> copyEntry(ReferenceEntry<K, V> original, ReferenceEntry<K, V> newNext) {
        if (original.getKey() == null) {
            // key collected
            return null;
        }

        ValueReference<K, V> valueReference = original.getValueReference();
        V value = valueReference.get();
        if ((value == null) && valueReference.isActive()) {
            // value collected
            return null;
        }

        ReferenceEntry<K, V> newEntry = map.entryFactory.copyEntry(this, original, newNext);
        newEntry.setValueReference(valueReference.copyFor(this.valueReferenceQueue, value, newEntry));
        return newEntry;
    }

    /**
     * Sets a new value of an entry. Adds newly created entries at the end of the access queue.
     */

    public void setValue(ReferenceEntry<K, V> entry, K key, V value, long now) {
        ValueReference<K, V> previous = entry.getValueReference();
        int weight = map.weigher.weigh(key, value);
        checkState(weight >= 0, "Weights must be non-negative");
        // 新建一个值引用
        ValueReference<K, V> valueReference = map.valueStrength.referenceValue(this, entry, value, weight);
        entry.setValueReference(valueReference);
        recordWrite(entry, weight, now);
        previous.notifyNewValue(value);
    }

    // loading

    public V get(K key, int hash, CacheLoader<? super K, V> loader) throws ExecutionException {
        checkNotNull(key);
        checkNotNull(loader);
        try {
            if (count != 0) { // read-volatile
                // don't call getLiveEntry, which would ignore loading values
                ReferenceEntry<K, V> e = getEntry(key, hash);
                if (e != null) {
                    long now = map.ticker.read();
                    V value = getLiveValue(e, now);
                    if (value != null) {
                        recordRead(e, now);
                        statsCounter.recordHits(1);
                        return scheduleRefresh(e, key, hash, value, now, loader);
                    }
                    ValueReference<K, V> valueReference = e.getValueReference();
                    if (valueReference.isLoading()) {
                        return waitForLoadingValue(e, key, valueReference);
                    }
                }
            }

            // at this point e is either null or expired;
            return lockedGetOrLoad(key, hash, loader);
        } catch (ExecutionException ee) {
            Throwable cause = ee.getCause();
            if (cause instanceof Error) {
                throw new ExecutionError((Error) cause);
            } else if (cause instanceof RuntimeException) {
                throw new UncheckedExecutionException(cause);
            }
            throw ee;
        } finally {
            postReadCleanup();
        }
    }
    //  根据key获取value：更新recencyQueue
    public V get(Object key, int hash) {
        try {
            if (count != 0) { // read-volatile
                long now = map.ticker.read();
                ReferenceEntry<K, V> e = getLiveEntry(key, hash, now);
                if (e == null) {
                    return null;
                }

                V value = e.getValueReference().get();
                if (value != null) {
                    recordRead(e, now);
                    return scheduleRefresh(e, e.getKey(), hash, value, now, map.defaultLoader);
                }
                tryDrainReferenceQueues();
            }
            return null;
        } finally {
            postReadCleanup();
        }
    }

    V lockedGetOrLoad(K key, int hash, CacheLoader<? super K, V> loader) throws ExecutionException {
        ReferenceEntry<K, V> e;
        ValueReference<K, V> valueReference = null;
        LoadingValueReference<K, V> loadingValueReference = null;
        boolean createNewEntry = true;//创建实体标识，当value为空或value已经过期

        lock();
        try {
            // re-read ticker once inside the lock
            long now = map.ticker.read();
            preWriteCleanup(now);//清理过期数据

            int newCount = this.count - 1;
            AtomicReferenceArray<ReferenceEntry<K, V>> table = this.table;
            int index = hash & (table.length() - 1);
            ReferenceEntry<K, V> first = table.get(index);

            // 寻找已经存在的key
            for (e = first; e != null; e = e.getNext()) {
                K entryKey = e.getKey();
                // 找到存在的key
                if (e.getHash() == hash && entryKey != null && map.keyEquivalence.equivalent(key, entryKey)) {
                    valueReference = e.getValueReference();
                    if (valueReference.isLoading()) {                           // 值正在导入中
                        createNewEntry = false;                                 // 不需要重复创建新值
                    } else {
                        V value = valueReference.get();
                        if (value == null) {                                    //已经被gc掉了，发生在弱引用上
                            enqueueNotification(entryKey, hash, value, valueReference.getWeight(), RemovalCause.COLLECTED);
                        } else if (map.isExpired(e, now)) {                     // 已经过期
                            // This is a duplicate check, as preWriteCleanup already purged expired
                            // entries, but let's accomodate an incorrect expiration queue.
                            enqueueNotification(entryKey, hash, value, valueReference.getWeight(), RemovalCause.EXPIRED);
                        } else {                                                // 正常使用中
                            recordLockedRead(e, now);                           // 记录最新的访问时间并添加到访问队列中，并返回
                            statsCounter.recordHits(1);
                            // we were concurrent with loading; don't consider refresh
                            return value;
                        }
                        // 处理无效数据：先删除访问及写队列，并将总数减1
                        // immediately reuse invalid entries
                        writeQueue.remove(e);
                        accessQueue.remove(e);
                        this.count = newCount; // write-volatile
                    }
                    break;
                }
            }

            // 第一次创建key
            if (createNewEntry) {
                loadingValueReference = new LoadingValueReference<>();

                if (e == null) {
                    e = newEntry(key, hash, first);             // e放在第一个，first放在e的后面
                    e.setValueReference(loadingValueReference); // 设置值引用为一个正在导入的引用
                    table.set(index, e);                        // 把e放在数组对应index的第一个
                } else {
                    e.setValueReference(loadingValueReference);
                }
            }
        } finally {
            unlock();
            postWriteCleanup();
        }
        // 第一次创建key时，异步导入值 & 放入读写队列
        if (createNewEntry) {
            try {
                // Synchronizes on the entry to allow failing fast when a recursive load is
                // detected. This may be circumvented when an entry is copied, but will fail fast most
                // of the time.
                synchronized (e) {
                    return loadSync(key, hash, loadingValueReference, loader);
                }
            } finally {
                statsCounter.recordMisses(1);
            }
        } else {
            // The entry already exists. Wait for loading.
            return waitForLoadingValue(e, key, valueReference);
        }
    }

    V waitForLoadingValue(ReferenceEntry<K, V> e, K key, ValueReference<K, V> valueReference) throws ExecutionException {
        if (!valueReference.isLoading()) {
            throw new AssertionError();
        }

        checkState(!Thread.holdsLock(e), "Recursive load of: %s", key);
        // don't consider expiration as we're concurrent with loading
        try {
            V value = valueReference.waitForValue();
            if (value == null) {
                throw new CacheLoader.InvalidCacheLoadException("CacheLoader returned null for key " + key + ".");
            }
            // re-read ticker now that loading has completed
            long now = map.ticker.read();
            recordRead(e, now);
            return value;
        } finally {
            statsCounter.recordMisses(1);
        }
    }

    V compute(K key, int hash, BiFunction<? super K, ? super V, ? extends V> function) {
        ReferenceEntry<K, V> e;
        ValueReference<K, V> valueReference = null;
        LoadingValueReference<K, V> loadingValueReference = null;
        boolean createNewEntry = true;
        V newValue;

        lock();
        try {
            // re-read ticker once inside the lock
            long now = map.ticker.read();
            preWriteCleanup(now);

            AtomicReferenceArray<ReferenceEntry<K, V>> table = this.table;
            int index = hash & (table.length() - 1);
            ReferenceEntry<K, V> first = table.get(index);

            for (e = first; e != null; e = e.getNext()) {
                K entryKey = e.getKey();
                if (e.getHash() == hash
                        && entryKey != null
                        && map.keyEquivalence.equivalent(key, entryKey)) {
                    valueReference = e.getValueReference();
                    if (map.isExpired(e, now)) {
                        // This is a duplicate check, as preWriteCleanup already purged expired
                        // entries, but let's accomodate an incorrect expiration queue.
                        enqueueNotification(
                                entryKey,
                                hash,
                                valueReference.get(),
                                valueReference.getWeight(),
                                RemovalCause.EXPIRED);
                    }

                    // immediately reuse invalid entries
                    writeQueue.remove(e);
                    accessQueue.remove(e);
                    createNewEntry = false;
                    break;
                }
            }

            // note valueReference can be an existing value or even itself another loading value if
            // the value for the key is already being computed.
            loadingValueReference = new LoadingValueReference<>(valueReference);

            if (e == null) {
                createNewEntry = true;
                e = newEntry(key, hash, first);
                e.setValueReference(loadingValueReference);
                table.set(index, e);
            } else {
                e.setValueReference(loadingValueReference);
            }

            newValue = loadingValueReference.compute(key, function);
            if (newValue != null) {
                try {
                    return getAndRecordStats(
                            key, hash, loadingValueReference, Futures.immediateFuture(newValue));
                } catch (ExecutionException exception) {
                    throw new AssertionError("impossible; Futures.immediateFuture can't throw");
                }
            } else if (createNewEntry) {
                removeLoadingValue(key, hash, loadingValueReference);
                return null;
            } else {
                removeEntry(e, hash, RemovalCause.EXPLICIT);
                return null;
            }
        } finally {
            unlock();
            postWriteCleanup();
        }
    }

    // at most one of loadSync/loadAsync may be called for any given LoadingValueReference

    V loadSync(
            K key,
            int hash,
            LoadingValueReference<K, V> loadingValueReference,
            CacheLoader<? super K, V> loader)
            throws ExecutionException {
        ListenableFuture<V> loadingFuture = loadingValueReference.loadFuture(key, loader);
        return getAndRecordStats(key, hash, loadingValueReference, loadingFuture);
    }

    ListenableFuture<V> loadAsync(
            final K key,
            final int hash,
            final LoadingValueReference<K, V> loadingValueReference,
            CacheLoader<? super K, V> loader) {
        final ListenableFuture<V> loadingFuture = loadingValueReference.loadFuture(key, loader);
        loadingFuture.addListener(
                new Runnable() {
                    @Override
                    public void run() {
                        try {
                            getAndRecordStats(key, hash, loadingValueReference, loadingFuture);
                        } catch (Throwable t) {
                            logger.warn("Exception thrown during refresh, msg = {}", t.getMessage(), t);
                            loadingValueReference.setException(t);
                        }
                    }
                },
                directExecutor());
        return loadingFuture;
    }

    /**
     * Waits uninterruptibly for {@code newValue} to be loaded, and then records loading stats.
     */
    V getAndRecordStats(
            K key,
            int hash,
            LoadingValueReference<K, V> loadingValueReference,
            ListenableFuture<V> newValue)
            throws ExecutionException {
        V value = null;
        try {
            value = getUninterruptibly(newValue);// 获取值且不可中断
            if (value == null) {
                throw new CacheLoader.InvalidCacheLoadException("CacheLoader returned null for key " + key + ".");
            }
            statsCounter.recordLoadSuccess(loadingValueReference.elapsedNanos());
            storeLoadedValue(key, hash, loadingValueReference, value);
            return value;
        } finally {
            if (value == null) {
                statsCounter.recordLoadException(loadingValueReference.elapsedNanos());
                removeLoadingValue(key, hash, loadingValueReference);
            }
        }
    }

    V scheduleRefresh(
            ReferenceEntry<K, V> entry,
            K key,
            int hash,
            V oldValue,
            long now,
            CacheLoader<? super K, V> loader) {
        if (map.refreshes()
                && (now - entry.getWriteTime() > map.refreshNanos)
                && !entry.getValueReference().isLoading()) {
            V newValue = refresh(key, hash, loader, true);
            if (newValue != null) {
                return newValue;
            }
        }
        return oldValue;
    }

    /**
     * Refreshes the value associated with {@code key}, unless another thread is already doing so.
     * Returns the newly refreshed value associated with {@code key} if it was refreshed inline, or
     * {@code null} if another thread is performing the refresh or if an error occurs during
     * refresh.
     */

    public V refresh(K key, int hash, CacheLoader<? super K, V> loader, boolean checkTime) {
        final LoadingValueReference<K, V> loadingValueReference = insertLoadingValueReference(key, hash, checkTime);
        if (loadingValueReference == null) {
            return null;
        }

        ListenableFuture<V> result = loadAsync(key, hash, loadingValueReference, loader);
        if (result.isDone()) {
            try {
                return Uninterruptibles.getUninterruptibly(result);
            } catch (Throwable t) {
                // don't let refresh exceptions propagate; error was already logged
            }
        }
        return null;
    }

    /**
     * Returns a newly inserted {@code LoadingValueReference}, or null if the live value reference
     * is already loading.
     */

    LoadingValueReference<K, V> insertLoadingValueReference(final K key, final int hash, boolean checkTime) {
        ReferenceEntry<K, V> e = null;
        lock();
        try {
            long now = map.ticker.read();
            preWriteCleanup(now);

            AtomicReferenceArray<ReferenceEntry<K, V>> table = this.table;
            int index = hash & (table.length() - 1);
            ReferenceEntry<K, V> first = table.get(index);

            // Look for an existing entry.
            for (e = first; e != null; e = e.getNext()) {
                K entryKey = e.getKey();
                if (e.getHash() == hash && entryKey != null && map.keyEquivalence.equivalent(key, entryKey)) {
                    // We found an existing entry.

                    ValueReference<K, V> valueReference = e.getValueReference();
                    if (valueReference.isLoading() || (checkTime && (now - e.getWriteTime() < map.refreshNanos))) {
                        // refresh is a no-op if loading is pending
                        // if checkTime, we want to check *after* acquiring the lock if refresh still needs
                        // to be scheduled
                        return null;
                    }

                    // continue returning old value while loading
                    ++modCount;
                    LoadingValueReference<K, V> loadingValueReference = new LoadingValueReference<>(valueReference);
                    e.setValueReference(loadingValueReference);
                    return loadingValueReference;
                }
            }

            ++modCount;
            LoadingValueReference<K, V> loadingValueReference = new LoadingValueReference<>();
            e = newEntry(key, hash, first);
            e.setValueReference(loadingValueReference);
            table.set(index, e);
            return loadingValueReference;
        } finally {
            unlock();
            postWriteCleanup();
        }
    }

    // reference queues, for garbage collection cleanup

    /**
     * 删除value(key已经被垃圾回收器回收)
     * Cleanup collected entries when the lock is available.
     */
    public void tryDrainReferenceQueues() {
        if (tryLock()) {
            try {
                drainReferenceQueues();
            } finally {
                unlock();
            }
        }
    }

    /**
     * 删除key已经被回收的value
     * Drain the key and value reference queues, cleaning up internal entries containing garbage
     * collected keys or values.
     */

    public void drainReferenceQueues() {
        if (map.usesKeyReferences()) {
            drainKeyReferenceQueue();
        }
        if (map.usesValueReferences()) {
            drainValueReferenceQueue();
        }
    }

    public void drainKeyReferenceQueue() {
        Reference<? extends K> ref;
        int i = 0;
        while ((ref = keyReferenceQueue.poll()) != null) {
            @SuppressWarnings("unchecked")
            ReferenceEntry<K, V> entry = (ReferenceEntry<K, V>) ref;
            map.reclaimKey(entry);
            if (++i == LocalCache.DRAIN_MAX) {
                break;
            }
        }
    }

    void drainValueReferenceQueue() {
        Reference<? extends V> ref;
        int i = 0;
        while ((ref = valueReferenceQueue.poll()) != null) {
            @SuppressWarnings("unchecked")
            ValueReference<K, V> valueReference = (ValueReference<K, V>) ref;
            map.reclaimValue(valueReference);
            if (++i == LocalCache.DRAIN_MAX) {
                break;
            }
        }
    }

    /**
     * Clears all entries from the key and value reference queues.
     */
    void clearReferenceQueues() {
        if (map.usesKeyReferences()) {
            clearKeyReferenceQueue();
        }
        if (map.usesValueReferences()) {
            clearValueReferenceQueue();
        }
    }

    void clearKeyReferenceQueue() {
        while (keyReferenceQueue.poll() != null) {
        }
    }

    void clearValueReferenceQueue() {
        while (valueReferenceQueue.poll() != null) {
        }
    }

    // recency queue, shared by expiration and eviction

    /**
     * Records the relative order in which this read was performed by adding {@code entry} to the
     * recency queue. At write-time, or when the queue is full past the threshold, the queue will be
     * drained and the entries therein processed.
     *
     * <p>Note: locked reads should use {@link #recordLockedRead}.
     */
    public void recordRead(ReferenceEntry<K, V> entry, long now) {
        if (map.recordsAccess()) {
            entry.setAccessTime(now);
        }
        recencyQueue.add(entry);
    }

    /**
     * Updates the eviction metadata that {@code entry} was just read. This currently amounts to
     * adding {@code entry} to relevant eviction lists.
     *
     * <p>Note: this method should only be called under lock, as it directly manipulates the
     * eviction queues. Unlocked reads should use {@link #recordRead}.
     */

    public void recordLockedRead(ReferenceEntry<K, V> entry, long now) {
        if (map.recordsAccess()) {
            entry.setAccessTime(now);
        }
        accessQueue.add(entry);
    }

    /**
     * Updates eviction metadata that {@code entry} was just written.
     * This currently amounts to adding {@code entry} to relevant eviction lists.
     */
    public void recordWrite(ReferenceEntry<K, V> entry, int weight, long now) {
        // we are already under lock, so drain the recency queue immediately
        drainRecencyQueue();
        totalWeight += weight;

        if (map.recordsAccess()) {
            entry.setAccessTime(now);
        }
        if (map.recordsWrite()) {
            entry.setWriteTime(now);
        }
        accessQueue.add(entry);
        writeQueue.add(entry);
    }

    /**
     * 从 accessQueue 移到 accessQueue
     *
     * Drains the recency queue, updating eviction metadata that the entries therein were read in
     * the specified relative order. This currently amounts to adding them to relevant eviction
     * lists (accounting for the fact that they could have been removed from the map since being
     * added to the recency queue).
     */
    public void drainRecencyQueue() {
        ReferenceEntry<K, V> e;
        while ((e = recencyQueue.poll()) != null) {
            // An entry may be in the recency queue despite it being removed from
            // the map . This can occur when the entry was concurrently read while a
            // writer is removing it from the segment or after a clear has removed
            // all of the segment's entries.
            if (accessQueue.contains(e)) {
                accessQueue.add(e);
            }
        }
    }

    // expiration

    /**
     * Cleanup expired entries when the lock is available.
     */
    public void tryExpireEntries(long now) {
        if (tryLock()) {
            try {
                expireEntries(now);
            } finally {
                unlock();
                // don't call postWriteCleanup as we're in a read
            }
        }
    }

    // 删除过期(队列)数据
    // 1、从 accessQueue 移到 accessQueue
    // 2、删除 writeQueue 中过期数据
    // 3、删除 accessQueue 中过期数据
    public void expireEntries(long now) {
        // 从 accessQueue 移到 accessQueue
        drainRecencyQueue();

        ReferenceEntry<K, V> e;
        while ((e = writeQueue.peek()) != null && map.isExpired(e, now)) {
            if (!removeEntry(e, e.getHash(), RemovalCause.EXPIRED)) {
                throw new AssertionError();
            }
        }
        while ((e = accessQueue.peek()) != null && map.isExpired(e, now)) {
            if (!removeEntry(e, e.getHash(), RemovalCause.EXPIRED)) {
                throw new AssertionError();
            }
        }
    }

    // eviction

    void enqueueNotification(K key, int hash, V value, int weight, RemovalCause cause) {
        totalWeight -= weight;
        if (cause.wasEvicted()) {
            statsCounter.recordEviction();
        }
        if (map.removalNotificationQueue != LocalCache.DISCARDING_QUEUE) {
            RemovalNotification<K, V> notification = RemovalNotification.create(key, value, cause);
            map.removalNotificationQueue.offer(notification);
        }
    }

    /**
     * Performs eviction if the segment is over capacity. Avoids flushing the entire lruk if the
     * newest entry exceeds the maximum weight all on its own.
     *
     * @param newest the most recently added entry
     */

    void evictEntries(ReferenceEntry<K, V> newest) {
        if (!map.evictsBySize()) {
            return;
        }

        drainRecencyQueue();
        // 当前新增的value本身的weight超过最大的权重时，删除该value
        // If the newest entry by itself is too heavy for the segment, don't bother evicting anything else, just that
        if (newest.getValueReference().getWeight() > maxSegmentWeight) {
            if (!removeEntry(newest, newest.getHash(), RemovalCause.SIZE)) {
                throw new AssertionError();
            }
        }

        //淘汰访问队列中队首的那个
        while (totalWeight > maxSegmentWeight) {
            ReferenceEntry<K, V> e = getNextEvictable();
            if (!removeEntry(e, e.getHash(), RemovalCause.SIZE)) {
                throw new AssertionError();
            }
        }
    }

    // TODO(fry): instead implement this with an eviction head

    ReferenceEntry<K, V> getNextEvictable() {
        for (ReferenceEntry<K, V> e : accessQueue) {
            int weight = e.getValueReference().getWeight();
            if (weight > 0) {
                return e;
            }
        }
        throw new AssertionError();
    }

    /**
     * Returns first entry of bin for given hash.
     */
    ReferenceEntry<K, V> getFirst(int hash) {
        // read this volatile field only once
        AtomicReferenceArray<ReferenceEntry<K, V>> table = this.table;
        return table.get(hash & (table.length() - 1));
    }

    // Specialized implementations of map methods

    public ReferenceEntry<K, V> getEntry(Object key, int hash) {
        for (ReferenceEntry<K, V> e = getFirst(hash); e != null; e = e.getNext()) {
            if (e.getHash() != hash) {
                continue;
            }

            K entryKey = e.getKey();
            if (entryKey == null) {
                tryDrainReferenceQueues();
                continue;
            }

            if (map.keyEquivalence.equivalent(key, entryKey)) {
                return e;
            }
        }

        return null;
    }

    public ReferenceEntry<K, V> getLiveEntry(Object key, int hash, long now) {
        ReferenceEntry<K, V> e = getEntry(key, hash);
        if (e == null) {
            return null;
        } else if (map.isExpired(e, now)) {
            tryExpireEntries(now);
            return null;
        }
        return e;
    }

    /**
     * Gets the value from an entry.
     * Returns null if the entry is invalid, partially-collected, loading, or expired.
     */
    public V getLiveValue(ReferenceEntry<K, V> entry, long now) {
        if (entry.getKey() == null) {
            tryDrainReferenceQueues();
            return null;
        }
        V value = entry.getValueReference().get();
        if (value == null) {
            tryDrainReferenceQueues();
            return null;
        }

        if (map.isExpired(entry, now)) {
            tryExpireEntries(now);
            return null;
        }
        return value;
    }

    public boolean containsKey(Object key, int hash) {
        try {
            if (count != 0) { // read-volatile
                long now = map.ticker.read();
                ReferenceEntry<K, V> e = getLiveEntry(key, hash, now);
                if (e == null) {
                    return false;
                }
                return e.getValueReference().get() != null;
            }

            return false;
        } finally {
            postReadCleanup();
        }
    }

    /**
     * This method is a convenience for testing. Code should call {@link LocalCache#containsValue}
     * directly.
     */
    @VisibleForTesting
    public boolean containsValue(Object value) {
        try {
            if (count != 0) { // read-volatile
                long now = map.ticker.read();
                AtomicReferenceArray<ReferenceEntry<K, V>> table = this.table;
                int length = table.length();
                for (int i = 0; i < length; ++i) {
                    for (ReferenceEntry<K, V> e = table.get(i); e != null; e = e.getNext()) {
                        V entryValue = getLiveValue(e, now);
                        if (entryValue == null) {
                            continue;
                        }
                        if (map.valueEquivalence.equivalent(value, entryValue)) {
                            return true;
                        }
                    }
                }
            }

            return false;
        } finally {
            postReadCleanup();
        }
    }

    public V put(K key, int hash, V value, boolean onlyIfAbsent) {
        lock();
        try {
            long now = map.ticker.read();
            preWriteCleanup(now);

            int newCount = this.count + 1;
            if (newCount > this.threshold) { // ensure capacity
                expand();
                newCount = this.count + 1;
            }

            AtomicReferenceArray<ReferenceEntry<K, V>> table = this.table;
            int index = hash & (table.length() - 1);
            ReferenceEntry<K, V> first = table.get(index);

            // Look for an existing entry.
            for (ReferenceEntry<K, V> e = first; e != null; e = e.getNext()) {
                K entryKey = e.getKey();
                if (e.getHash() == hash
                        && entryKey != null
                        && map.keyEquivalence.equivalent(key, entryKey)) {
                    // We found an existing entry.

                    ValueReference<K, V> valueReference = e.getValueReference();
                    V entryValue = valueReference.get();

                    if (entryValue == null) {
                        ++modCount;
                        if (valueReference.isActive()) {
                            enqueueNotification(key, hash, entryValue, valueReference.getWeight(), RemovalCause.COLLECTED);
                            setValue(e, key, value, now);
                            newCount = this.count; // count remains unchanged
                        } else {
                            setValue(e, key, value, now);
                            newCount = this.count + 1;
                        }
                        this.count = newCount; // write-volatile
                        evictEntries(e);
                        return null;
                    } else if (onlyIfAbsent) {
                        // Mimic
                        // "if (!map.containsKey(key)) ...
                        // else return map.get(key);
                        recordLockedRead(e, now);
                        return entryValue;
                    } else {
                        // clobber existing entry, count remains unchanged
                        ++modCount;
                        enqueueNotification(key, hash, entryValue, valueReference.getWeight(), RemovalCause.REPLACED);
                        setValue(e, key, value, now);
                        evictEntries(e);
                        return entryValue;
                    }
                }
            }

            // Create a new entry.
            ++modCount;
            ReferenceEntry<K, V> newEntry = newEntry(key, hash, first);
            setValue(newEntry, key, value, now);
            table.set(index, newEntry);
            newCount = this.count + 1;
            this.count = newCount; // write-volatile
            evictEntries(newEntry);
            return null;
        } finally {
            unlock();
            postWriteCleanup();
        }
    }

    /**
     * Expands the table if possible.
     * 扩容 容器
     */

    void expand() {
        AtomicReferenceArray<ReferenceEntry<K, V>> oldTable = table;
        int oldCapacity = oldTable.length();
        if (oldCapacity >= LocalCache.MAXIMUM_CAPACITY) {
            return;
        }

        /*
         * Reclassify nodes in each list to new Map. Because we are using power-of-two expansion, the
         * elements from each bin must either stay at same index, or move with a power of two offset.
         * We eliminate unnecessary node creation by catching cases where old nodes can be reused
         * because their next fields won't change. Statistically, at the default threshold, only about
         * one-sixth of them need cloning when a table doubles. The nodes they replace will be garbage
         * collectable as soon as they are no longer referenced by any reader thread that may be in
         * the midst of traversing table right now.
         */

        int newCount = count;
        AtomicReferenceArray<ReferenceEntry<K, V>> newTable = newEntryArray(oldCapacity << 1);
        threshold = newTable.length() * 3 / 4;
        int newMask = newTable.length() - 1;
        for (int oldIndex = 0; oldIndex < oldCapacity; ++oldIndex) {
            // We need to guarantee that any existing reads of old Map can
            // proceed. So we cannot yet null out each bin.
            ReferenceEntry<K, V> head = oldTable.get(oldIndex);

            if (head != null) {
                ReferenceEntry<K, V> next = head.getNext();
                int headIndex = head.getHash() & newMask;

                // Single node on list
                if (next == null) {
                    newTable.set(headIndex, head);
                } else {
                    // Reuse the consecutive sequence of nodes with the same target
                    // index from the end of the list. tail points to the first
                    // entry in the reusable list.
                    ReferenceEntry<K, V> tail = head;
                    int tailIndex = headIndex;
                    for (ReferenceEntry<K, V> e = next; e != null; e = e.getNext()) {
                        int newIndex = e.getHash() & newMask;
                        if (newIndex != tailIndex) {
                            // The index changed. We'll need to copy the previous entry.
                            tailIndex = newIndex;
                            tail = e;
                        }
                    }
                    newTable.set(tailIndex, tail);

                    // Clone nodes leading up to the tail.
                    for (ReferenceEntry<K, V> e = head; e != tail; e = e.getNext()) {
                        int newIndex = e.getHash() & newMask;
                        ReferenceEntry<K, V> newNext = newTable.get(newIndex);
                        ReferenceEntry<K, V> newFirst = copyEntry(e, newNext);
                        if (newFirst != null) {
                            newTable.set(newIndex, newFirst);
                        } else {
                            removeCollectedEntry(e);
                            newCount--;
                        }
                    }
                }
            }
        }
        table = newTable;
        this.count = newCount;
    }

    public boolean replace(K key, int hash, V oldValue, V newValue) {
        lock();
        try {
            long now = map.ticker.read();
            preWriteCleanup(now);

            AtomicReferenceArray<ReferenceEntry<K, V>> table = this.table;
            int index = hash & (table.length() - 1);
            ReferenceEntry<K, V> first = table.get(index);

            for (ReferenceEntry<K, V> e = first; e != null; e = e.getNext()) {
                K entryKey = e.getKey();
                if (e.getHash() == hash
                        && entryKey != null
                        && map.keyEquivalence.equivalent(key, entryKey)) {
                    ValueReference<K, V> valueReference = e.getValueReference();
                    V entryValue = valueReference.get();
                    if (entryValue == null) {
                        if (valueReference.isActive()) {
                            // If the value disappeared, this entry is partially collected.
                            int newCount = this.count - 1;
                            ++modCount;
                            ReferenceEntry<K, V> newFirst =
                                    removeValueFromChain(
                                            first,
                                            e,
                                            entryKey,
                                            hash,
                                            entryValue,
                                            valueReference,
                                            RemovalCause.COLLECTED);
                            newCount = this.count - 1;
                            table.set(index, newFirst);
                            this.count = newCount; // write-volatile
                        }
                        return false;
                    }

                    if (map.valueEquivalence.equivalent(oldValue, entryValue)) {
                        ++modCount;
                        enqueueNotification(key, hash, entryValue, valueReference.getWeight(), RemovalCause.REPLACED);
                        setValue(e, key, newValue, now);
                        evictEntries(e);
                        return true;
                    } else {
                        // Mimic
                        // "if (map.containsKey(key) && map.get(key).equals(oldValue))..."
                        recordLockedRead(e, now);
                        return false;
                    }
                }
            }

            return false;
        } finally {
            unlock();
            postWriteCleanup();
        }
    }

    public V replace(K key, int hash, V newValue) {
        lock();
        try {
            long now = map.ticker.read();
            preWriteCleanup(now);

            AtomicReferenceArray<ReferenceEntry<K, V>> table = this.table;
            int index = hash & (table.length() - 1);
            ReferenceEntry<K, V> first = table.get(index);

            for (ReferenceEntry<K, V> e = first; e != null; e = e.getNext()) {
                K entryKey = e.getKey();
                if (e.getHash() == hash
                        && entryKey != null
                        && map.keyEquivalence.equivalent(key, entryKey)) {
                    ValueReference<K, V> valueReference = e.getValueReference();
                    V entryValue = valueReference.get();
                    if (entryValue == null) {
                        if (valueReference.isActive()) {
                            // If the value disappeared, this entry is partially collected.
                            int newCount = this.count - 1;
                            ++modCount;
                            ReferenceEntry<K, V> newFirst =
                                    removeValueFromChain(
                                            first,
                                            e,
                                            entryKey,
                                            hash,
                                            entryValue,
                                            valueReference,
                                            RemovalCause.COLLECTED);
                            newCount = this.count - 1;
                            table.set(index, newFirst);
                            this.count = newCount; // write-volatile
                        }
                        return null;
                    }

                    ++modCount;
                    enqueueNotification(
                            key, hash, entryValue, valueReference.getWeight(), RemovalCause.REPLACED);
                    setValue(e, key, newValue, now);
                    evictEntries(e);
                    return entryValue;
                }
            }

            return null;
        } finally {
            unlock();
            postWriteCleanup();
        }
    }

    public V remove(Object key, int hash) {
        lock();
        try {
            long now = map.ticker.read();
            preWriteCleanup(now);

            int newCount = this.count - 1;
            AtomicReferenceArray<ReferenceEntry<K, V>> table = this.table;
            int index = hash & (table.length() - 1);
            ReferenceEntry<K, V> first = table.get(index);

            for (ReferenceEntry<K, V> e = first; e != null; e = e.getNext()) {
                K entryKey = e.getKey();
                if (e.getHash() == hash && entryKey != null && map.keyEquivalence.equivalent(key, entryKey)) {
                    ValueReference<K, V> valueReference = e.getValueReference();
                    V entryValue = valueReference.get();

                    RemovalCause cause;
                    if (entryValue != null) {
                        cause = RemovalCause.EXPLICIT;
                    } else if (valueReference.isActive()) {
                        cause = RemovalCause.COLLECTED;
                    } else {
                        // currently loading
                        return null;
                    }

                    ++modCount;
                    ReferenceEntry<K, V> newFirst = removeValueFromChain(first, e, entryKey, hash, entryValue, valueReference, cause);
                    newCount = this.count - 1;
                    table.set(index, newFirst);
                    this.count = newCount; // write-volatile
                    return entryValue;
                }
            }

            return null;
        } finally {
            unlock();
            postWriteCleanup();
        }
    }

    public boolean remove(Object key, int hash, Object value) {
        lock();
        try {
            long now = map.ticker.read();
            preWriteCleanup(now);

            int newCount = this.count - 1;
            AtomicReferenceArray<ReferenceEntry<K, V>> table = this.table;
            int index = hash & (table.length() - 1);
            ReferenceEntry<K, V> first = table.get(index);

            for (ReferenceEntry<K, V> e = first; e != null; e = e.getNext()) {
                K entryKey = e.getKey();
                if (e.getHash() == hash
                        && entryKey != null
                        && map.keyEquivalence.equivalent(key, entryKey)) {
                    ValueReference<K, V> valueReference = e.getValueReference();
                    V entryValue = valueReference.get();

                    RemovalCause cause;
                    if (map.valueEquivalence.equivalent(value, entryValue)) {
                        cause = RemovalCause.EXPLICIT;
                    } else if (entryValue == null && valueReference.isActive()) {
                        cause = RemovalCause.COLLECTED;
                    } else {
                        // currently loading
                        return false;
                    }

                    ++modCount;
                    ReferenceEntry<K, V> newFirst =
                            removeValueFromChain(first, e, entryKey, hash, entryValue, valueReference, cause);
                    newCount = this.count - 1;
                    table.set(index, newFirst);
                    this.count = newCount; // write-volatile
                    return (cause == RemovalCause.EXPLICIT);
                }
            }

            return false;
        } finally {
            unlock();
            postWriteCleanup();
        }
    }

    /**
     * 保存导入的值
     * 1、清理过期数据
     * 2、保存记录到 accessQueue
     *
     * @param key               key
     * @param hash              hash
     * @param oldValueReference 原来正在导入的值引用
     * @param newValue          新加载的值
     * @return
     */
    private boolean storeLoadedValue(K key, int hash, LoadingValueReference<K, V> oldValueReference, V newValue) {
        lock();
        try {
            long now = map.ticker.read();
            preWriteCleanup(now);

            int newCount = this.count + 1;
            if (newCount > this.threshold) { // ensure capacity
                expand();                   // 扩展原来的table
                newCount = this.count + 1;
            }

            AtomicReferenceArray<ReferenceEntry<K, V>> table = this.table;
            int index = hash & (table.length() - 1);
            ReferenceEntry<K, V> first = table.get(index);

            // 查找已经存在的引用实体
            for (ReferenceEntry<K, V> e = first; e != null; e = e.getNext()) {
                K entryKey = e.getKey();
                if (e.getHash() == hash && entryKey != null && map.keyEquivalence.equivalent(key, entryKey)) {
                    ValueReference<K, V> valueReference = e.getValueReference();
                    V entryValue = valueReference.get();
                    // replace the old LoadingValueReference if it's live, otherwise perform a putIfAbsent
                    if (oldValueReference == valueReference || (entryValue == null && valueReference != LocalCache.UNSET)) {
                        ++modCount;
                        if (oldValueReference.isActive()) {
                            RemovalCause cause = (entryValue == null) ? RemovalCause.COLLECTED : RemovalCause.REPLACED;
                            enqueueNotification(key, hash, entryValue, oldValueReference.getWeight(), cause);
                            newCount--;
                        }
                        setValue(e, key, newValue, now);//为存在的对象设置新值
                        this.count = newCount; // write-volatile
                        evictEntries(e);// 检查是否已经超过最多的数量，否则淘汰前面的老对象
                        return true;
                    }

                    // the loaded value was already clobbered
                    enqueueNotification(key, hash, newValue, 0, RemovalCause.REPLACED);
                    return false;
                }
            }

            // 不存在实体（一般第一次创建）
            ++modCount;
            ReferenceEntry<K, V> newEntry = newEntry(key, hash, first);
            setValue(newEntry, key, newValue, now);
            table.set(index, newEntry);
            this.count = newCount; // write-volatile
            evictEntries(newEntry);
            return true;
        } finally {
            unlock();
            postWriteCleanup();
        }
    }

    void clear() {
        if (count != 0) { // read-volatile
            lock();
            try {
                long now = map.ticker.read();
                preWriteCleanup(now);

                AtomicReferenceArray<ReferenceEntry<K, V>> table = this.table;
                for (int i = 0; i < table.length(); ++i) {
                    for (ReferenceEntry<K, V> e = table.get(i); e != null; e = e.getNext()) {
                        // Loading references aren't actually in the map yet.
                        if (e.getValueReference().isActive()) {
                            K key = e.getKey();
                            V value = e.getValueReference().get();
                            RemovalCause cause =
                                    (key == null || value == null) ? RemovalCause.COLLECTED : RemovalCause.EXPLICIT;
                            enqueueNotification(
                                    key, e.getHash(), value, e.getValueReference().getWeight(), cause);
                        }
                    }
                }
                for (int i = 0; i < table.length(); ++i) {
                    table.set(i, null);
                }
                clearReferenceQueues();
                writeQueue.clear();
                accessQueue.clear();
                readCount.set(0);

                ++modCount;
                count = 0; // write-volatile
            } finally {
                unlock();
                postWriteCleanup();
            }
        }
    }

    private ReferenceEntry<K, V> removeValueFromChain(
            ReferenceEntry<K, V> first,
            ReferenceEntry<K, V> entry,
            K key,
            int hash,
            V value,
            ValueReference<K, V> valueReference,
            RemovalCause cause) {
        enqueueNotification(key, hash, value, valueReference.getWeight(), cause);
        writeQueue.remove(entry);
        accessQueue.remove(entry);

        if (valueReference.isLoading()) {
            valueReference.notifyNewValue(null);
            return first;
        } else {
            return removeEntryFromChain(first, entry);
        }
    }

    private ReferenceEntry<K, V> removeEntryFromChain(ReferenceEntry<K, V> first, ReferenceEntry<K, V> entry) {
        int newCount = count;
        ReferenceEntry<K, V> newFirst = entry.getNext();
        for (ReferenceEntry<K, V> e = first; e != entry; e = e.getNext()) {
            ReferenceEntry<K, V> next = copyEntry(e, newFirst);
            if (next != null) {
                newFirst = next;
            } else {
                removeCollectedEntry(e);
                newCount--;
            }
        }
        this.count = newCount;
        return newFirst;
    }

    private void removeCollectedEntry(ReferenceEntry<K, V> entry) {
        enqueueNotification(
                entry.getKey(),
                entry.getHash(),
                entry.getValueReference().get(),
                entry.getValueReference().getWeight(),
                RemovalCause.COLLECTED);
        writeQueue.remove(entry);
        accessQueue.remove(entry);
    }

    /**
     * 删除entry(key被回收了)
     * Removes an entry whose key has been garbage collected.
     */
    public boolean reclaimKey(ReferenceEntry<K, V> entry, int hash) {
        lock();
        try {
            int newCount = count - 1;
            AtomicReferenceArray<ReferenceEntry<K, V>> table = this.table;
            int index = hash & (table.length() - 1);
            ReferenceEntry<K, V> first = table.get(index);

            for (ReferenceEntry<K, V> e = first; e != null; e = e.getNext()) {
                if (e == entry) {
                    ++modCount;
                    ReferenceEntry<K, V> newFirst =
                            removeValueFromChain(
                                    first,
                                    e,
                                    e.getKey(),
                                    hash,
                                    e.getValueReference().get(),
                                    e.getValueReference(),
                                    RemovalCause.COLLECTED);
                    newCount = this.count - 1;
                    table.set(index, newFirst);
                    this.count = newCount; // write-volatile
                    return true;
                }
            }

            return false;
        } finally {
            unlock();
            postWriteCleanup();
        }
    }

    /**
     * Removes an entry whose value has been garbage collected.
     */
    public boolean reclaimValue(K key, int hash, ValueReference<K, V> valueReference) {
        lock();
        try {
            int newCount = this.count - 1;
            AtomicReferenceArray<ReferenceEntry<K, V>> table = this.table;
            int index = hash & (table.length() - 1);
            ReferenceEntry<K, V> first = table.get(index);

            for (ReferenceEntry<K, V> e = first; e != null; e = e.getNext()) {
                K entryKey = e.getKey();
                if (e.getHash() == hash
                        && entryKey != null
                        && map.keyEquivalence.equivalent(key, entryKey)) {
                    ValueReference<K, V> v = e.getValueReference();
                    if (v == valueReference) {
                        ++modCount;
                        ReferenceEntry<K, V> newFirst =
                                removeValueFromChain(
                                        first,
                                        e,
                                        entryKey,
                                        hash,
                                        valueReference.get(),
                                        valueReference,
                                        RemovalCause.COLLECTED);
                        newCount = this.count - 1;
                        table.set(index, newFirst);
                        this.count = newCount; // write-volatile
                        return true;
                    }
                    return false;
                }
            }

            return false;
        } finally {
            unlock();
            if (!isHeldByCurrentThread()) { // don't cleanup inside of put
                postWriteCleanup();
            }
        }
    }

    private boolean removeLoadingValue(K key, int hash, LoadingValueReference<K, V> valueReference) {
        lock();
        try {
            AtomicReferenceArray<ReferenceEntry<K, V>> table = this.table;
            int index = hash & (table.length() - 1);
            ReferenceEntry<K, V> first = table.get(index);

            for (ReferenceEntry<K, V> e = first; e != null; e = e.getNext()) {
                K entryKey = e.getKey();
                if (e.getHash() == hash
                        && entryKey != null
                        && map.keyEquivalence.equivalent(key, entryKey)) {
                    ValueReference<K, V> v = e.getValueReference();
                    if (v == valueReference) {
                        if (valueReference.isActive()) {
                            e.setValueReference(valueReference.getOldValue());
                        } else {
                            ReferenceEntry<K, V> newFirst = removeEntryFromChain(first, e);
                            table.set(index, newFirst);
                        }
                        return true;
                    }
                    return false;
                }
            }

            return false;
        } finally {
            unlock();
            postWriteCleanup();
        }
    }

    // 删除 entry
    // 删除 accessQueue
    private boolean removeEntry(ReferenceEntry<K, V> entry, int hash, RemovalCause cause) {
        int newCount = this.count - 1;
        AtomicReferenceArray<ReferenceEntry<K, V>> xtable = this.table;
        int index = hash & (table.length() - 1);
        ReferenceEntry<K, V> first = table.get(index);

        for (ReferenceEntry<K, V> e = first; e != null; e = e.getNext()) {
            if (e == entry) {
                ++modCount;
                ReferenceEntry<K, V> newFirst =
                        removeValueFromChain(
                                first,
                                e,
                                e.getKey(),
                                hash,
                                e.getValueReference().get(),
                                e.getValueReference(),
                                cause);
                newCount = this.count - 1;
                table.set(index, newFirst);
                this.count = newCount; // write-volatile
                return true;
            }
        }

        return false;
    }

    /**
     * Performs routine cleanup following a read. Normally cleanup happens during writes.
     * If cleanup is not observed after a sufficient number of reads, try cleaning up from the read thread.
     */
    public void postReadCleanup() {
        if ((readCount.incrementAndGet() & LocalCache.DRAIN_THRESHOLD) == 0) {
            cleanUp();
        }
    }

    /**
     * Performs routine cleanup prior to executing a write. This should be called every time a write
     * thread acquires the segment lock, immediately after acquiring the lock.
     *
     * <p>Post-condition: expireEntries has been run.
     */

    private void preWriteCleanup(long now) {
        runLockedCleanup(now);
    }

    /**
     * Performs routine cleanup following a write.
     */
    public void postWriteCleanup() {
        runUnlockedCleanup();
    }

    public void cleanUp() {
        long now = map.ticker.read();
        runLockedCleanup(now);
        runUnlockedCleanup();
    }

    private void runLockedCleanup(long now) {
        if (tryLock()) {
            try {
                drainReferenceQueues();//只有非强引用时才有效
                expireEntries(now); // 删除过期的对象
                readCount.set(0);
            } finally {
                unlock();
            }
        }
    }

    private void runUnlockedCleanup() {
        // locked cleanup may generate notifications we can send unlocked
        if (!isHeldByCurrentThread()) {
            map.processPendingNotifications();
        }
    }
}
