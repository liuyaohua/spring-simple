package com.example.liuyaohua.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

@ApiModel(value = "userInfo", description = "用户信息")
public class User {
    @ApiModelProperty(value = "id", required = true, example = "1000", allowableValues = "1,100")
    private int id;
    @ApiModelProperty(value = "用户名", required = true, example = "张三")
    private String name;
    @ApiModelProperty(value = "生日", required = true, example = "日期")
    private Date birth;
    @ApiModelProperty(value = "用户状态", required = true, example = "0", allowableValues = "0,2")
    private UserStatusEnum status;
    @ApiModelProperty(value = "用户状态", required = false, example = "true", allowableValues = "true|false")
    private boolean testFlag2;


    @ApiModelProperty(value = "用户状态", hidden = true)
    private int flag = 0;

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return "User [id=" + id + ", name=" + name + ", birth=" + birth
                + ", status=" + status + "]";
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonSerialize(using = JsonDateSerializer.class)
    public Date getBirth() {
        return birth;
    }

    @JsonDeserialize(using = JsonDateDeserializer.class)
    public void setBirth(Date birth) {
        this.birth = birth;
    }

    public UserStatusEnum getStatus() {
        return status;
    }

    public void setStatus(UserStatusEnum status) {
        this.status = status;
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public boolean isTestFlag2() {
        return testFlag2;
    }

    public void setTestFlag2(boolean testFlag2) {
        this.testFlag2 = testFlag2;
    }
}
