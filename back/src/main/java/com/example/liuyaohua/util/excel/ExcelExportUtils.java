package com.example.liuyaohua.util.excel;

import com.google.common.base.Strings;
import org.apache.poi.hssf.usermodel.DVConstraint;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.xssf.usermodel.*;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * 作者 yaohua.liu
 * 日期 2016-08-25 15:06
 * 说明 导出
 */
public class ExcelExportUtils {

    /**
     * 设置单元格上提示
     *
     * @param sheet         要设置的sheet.
     * @param promptTitle   标题
     * @param promptContent 内容
     * @param firstRow      开始行
     * @param endRow        结束行
     * @param firstCol      开始列
     * @param endCol        结束列
     * @return 设置好的sheet.
     */
    public static XSSFSheet setHSSFPrompt(XSSFSheet sheet, String promptTitle, String promptContent, int firstRow, int endRow, int firstCol, int endCol) {
        // excel 2007
        // XSSFSheet sheet = workbook.createSheet("Data Validation");
        XSSFDataValidationHelper dvHelper = new XSSFDataValidationHelper(sheet);
        XSSFDataValidationConstraint dvConstraint = (XSSFDataValidationConstraint) dvHelper.createNumericConstraint(DVConstraint.ValidationType.INTEGER, DVConstraint.OperatorType.BETWEEN, "-1", "2");

        CellRangeAddressList cellRangeAddressList = new CellRangeAddressList(firstRow, endRow, firstCol, endCol);
        XSSFDataValidation validation = (XSSFDataValidation) dvHelper.createValidation(dvConstraint, cellRangeAddressList);
        validation.createPromptBox(promptTitle, promptContent);
        validation.setShowPromptBox(true);
        sheet.addValidationData(validation);

        return sheet;
    }

    /***
     * 创建Excel的workbook.用于输出到其它流中.如网络.文件.
     *
     * @param data 待处理的数据
     * @param <T>  特定数据类型
     * @return 处理好的WorkBook
     */
    public static <T> Workbook buildWorkBook(String[][] data) {
        if (data == null) {
            return null;
        }
        Workbook workbook = new XSSFWorkbook();
        //ExcelSheet excelSheet = clz.getAnnotation(ExcelSheet.class);

        // 标题栏样式.
        XSSFCellStyle titleStyle = (XSSFCellStyle) workbook.createCellStyle();
        // 标题栏字体.
        Font font = workbook.createFont();
        font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
        // 字体颜色:@See http://bbs.csdn.net/topics/360067530
        font.setColor(HSSFColor.WHITE.index);

        titleStyle.setFont(font);
        // 水平居中
        titleStyle.setAlignment(CellStyle.ALIGN_CENTER);
        // 垂直居中
        titleStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
        //
        titleStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
        // 设置前景色
        titleStyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(0, 176, 80)));

        CellStyle cellStyle = workbook.createCellStyle();

        // 水平居中
        cellStyle.setAlignment(CellStyle.ALIGN_CENTER);
        cellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);

        String sheetName = "sheet1";
        /*if (excelSheet != null) {
            if (!StringUtils.isEmpty(excelSheet.sheetName())) {
                sheetName = excelSheet.sheetName();
            }else if (!StringUtils.isEmpty(excelSheet.value())) {
                sheetName = excelSheet.value();
            }
        }*/
        Sheet sheet = workbook.createSheet(sheetName);
        sheet.setDefaultColumnWidth(18);

        createHeader(titleStyle, sheet, data[0]);
        createBody(cellStyle, sheet, data);
        return workbook;
    }

    /**
     * 创建 Excel 的 Header 方法
     *
     * @param sheet 待添加Header的Excel的Sheet
     * @param <T>   特定类型的Excel数据类
     */
    private static <T> void createHeader(CellStyle titleStyle, Sheet sheet, String[] title) {
        Row header = sheet.createRow(0);
        if (title == null || title.length == 0) {
            return;
        }
        for (int i = 0; i < title.length; i++) {
            sheet.setColumnWidth(i, sheet.getDefaultColumnWidth() * 256);
            Cell headerCell = header.createCell(i, Cell.CELL_TYPE_STRING);
            headerCell.setCellValue(title[i]);
            headerCell.setCellStyle(titleStyle);
        }
    }

    private static <T> void createBody(CellStyle cellStyle, Sheet sheet, String[][] data) {
        if (data == null || data.length == 0) {
            return;
        }
        int rowIndex = 1;
        int rows = data.length;
        for (int i = 1; i < rows; i++) {
            // 创建一行数据
            Row sheetRow = sheet.createRow(rowIndex);
            rowIndex++;
            //String fields[] = rowData.getClass().getDeclaredFields();
            int order = 0;
            for (String f : data[i]) {
                String value = Strings.isNullOrEmpty(f) ? "" : f;
                Cell sheetCell = sheetRow.createCell(order);
                order++;
                sheetCell.setCellStyle(cellStyle);
                sheetCell.setCellValue(value);
            }
        }
    }

    /**
     * 导出Excel数据到文件
     *
     * @param data     数据列表,数据需要使用注解标明
     * @param fileName 文件路径-文件名
     * @param <T>      数据类型
     * @throws IOException io异常
     */
    public static <T> void exportExcel(String[][] data, String fileName) throws IOException {
        Workbook workbook = buildWorkBook(data);
        FileOutputStream out = new FileOutputStream(fileName);
        workbook.write(out);
        out.flush();
        out.close();
    }

    /**
     * 导出Excel到网络下载.
     *
     * @param data 待处理数据列表
     * @param clz 数据类型
     * @param fileName 文件名称
     * @param request 请求
     * @param response 响应
     * @param <T> 类型
     * @throws IOException
     */
   /* public static <T> void exportExcel(List<T> data, Class<T> clz, String fileName, HttpServletRequest request,
            HttpServletResponse response) throws IOException {
        response.reset();
        String agent = request.getHeader("USER-AGENT");
        OutputStream out = response.getOutputStream();
        String tFileName = "导出文件";

        ExcelSheet excelSheet = clz.getAnnotation(ExcelSheet.class);
        if (excelSheet != null && StringUtils.isNotBlank(excelSheet.fileName())) {
            tFileName = excelSheet.fileName();
        }else if (StringUtils.isNotBlank(fileName)) {
            tFileName = fileName;
        }

        String encodeFileName = encodeFileName(agent, tFileName);
        Workbook workbook = buildWorkBook(data, clz);
        response.setContentType("application/msexcel;");
        response.setHeader("Content-disposition", "attachment; filename=" + encodeFileName + ".xlsx");
        workbook.write(out);
        out.flush();
        out.close();
    }*/

    /**
     * 根据浏览器agent编码文件名
     *
     * @param agent
     * @param fileName
     * @return
     * @throws UnsupportedEncodingException
     */
    public static String encodeFileName(String agent, String fileName) throws UnsupportedEncodingException {
        if (agent != null && agent.contains("MSIE")) {
            fileName = URLEncoder.encode(fileName, "UTF8");
            fileName = fileName.replace("+", "%20");
        } else {
            fileName = new String(fileName.getBytes("UTF-8"), "ISO-8859-1");
        }
        return fileName;
    }

}
