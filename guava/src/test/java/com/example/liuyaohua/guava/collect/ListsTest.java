package com.example.liuyaohua.guava.collect;

import com.google.common.base.Function;
import com.google.common.base.Objects;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * 作者 yaohua.liu
 * 日期 2014-11-20 16:50
 * * 说明 ...
 */
public class ListsTest {

    @Test
    public void testLists(){

        //创建列表。还可以这样 Lists.newLinkedList()
        // 创建list 在初始化时可以添加值,也可以是Iterable/Iterator
        ArrayList list= Lists.newArrayList();
        ArrayList list1= Lists.newArrayList("1","2");
        ArrayList list2= Lists.newArrayList(Splitter.on(",").split("a,b").iterator());

        //asList 返回一个不可修改的List，按参数的先后顺序放入新的list中
        List<String> list3=Lists.asList("1","b",new String[]{"a"});
        String result=list3.toString();

        // 把list3按size为2平均分成n个list
        List<List<String>> lists=Lists.partition(list3, 2);
        String result1=lists.toString();

        // 反转列表
        List<String> reverse=Lists.reverse(list3);
        String result2=reverse.toString();

        // 列表转化，把原列表里面的元素转换成一个新的对象列表
        // 可用于把前端的数据转换成新的对象
        final List<Integer> in = Lists.transform(list3, new Function<String, Integer>() {
            @Override
            public Integer apply(String input) {
                if (Objects.equal(input, "a")) {
                    return 20;
                }else
                    return 0;
            }
        });
        String result3=in.toString();
    }
}
