package com.example.liuyaohua.guava.concurrent;

import com.google.common.base.Function;
import com.google.common.util.concurrent.*;
import org.junit.Test;
import org.omg.CORBA.portable.ApplicationException;

import java.util.concurrent.Callable;
import java.util.concurrent.Executors;

/**
 * 作者 yaohua.liu
 * 日期 2014-11-26 17:23
 * * 说明 参考资料：http://blog.firdau.si/2010/07/07/guava-using-checkedfuture/
 */
public class CheckedFutureTest {

    @Test
    public void test() throws ApplicationException {

        ListeningExecutorService listeningExecutorService = MoreExecutors.listeningDecorator(Executors.newFixedThreadPool(10));
        ListenableFuture task = listeningExecutorService.submit(new Callable() {
            @Override
            public Object call() throws Exception {
                return "call";
            }
        });

        Function<Exception, ApplicationException> mapper =new Function<Exception, ApplicationException>() {
                    @Override
                    public ApplicationException apply( Exception from) {
                        if (from.getCause() instanceof ApplicationException) {
                            try {
                                throw (ApplicationException) from.getCause();
                            } catch (ApplicationException e) {
                                e.printStackTrace();
                            }
                        }
                        return new ApplicationException("myException",null);
                    }
                };
        CheckedFuture<String, ApplicationException> checkedTask = Futures.makeChecked(task, mapper);

        checkedTask.checkedGet();
    }
}
