package com.example.liuyaohua.unit.controller;

import com.example.liuyaohua.controller.UserController;
import com.example.liuyaohua.unit.CommonTest;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

/**
 * 作者 yaohua.liu
 * 日期 2017-04-05 17:56
 * 说明 ...
 */
public class TestControllerTest extends CommonTest {

	private MockMvc mockMvc;

	@Before
	public void setUp() throws Exception {
		//mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
		mockMvc = standaloneSetup(new UserController())
				.defaultRequest(get("/")
						.contextPath("")
						.servletPath("/user/user.json")
						.accept(MediaType.APPLICATION_JSON))
				.build();
	}

	@Test
	public void testTestToken() throws Exception {
		ResultActions actions = mockMvc.perform(get("/user/user.json").accept(MediaType.parseMediaType("application/json;charset=UTF-8")));
		actions.andExpect(status().isOk())
				.andExpect(content().contentType("application/json;charset=UTF-8"))
                /*.andExpect(content().json("{'foo':'bar'}"))*/;
	}

}