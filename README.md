源码与博客对应url如下：
1、lruk-cache：基于LRU-K算法设计本地缓存实现流量削峰
https://blog.csdn.net/love254443233/article/details/82598381
2、table-ajax：ajax异步获取数据后动态向表格中添加数据(行)
https://blog.csdn.net/love254443233/article/details/48468397
3、dubbo-test：单元测试(junit+dubbo+mockito)
https://blog.csdn.net/love254443233/article/details/82468510
4、swagger：SpringMvc 3分钟集成swagger2
https://blog.csdn.net/love254443233/article/details/53069194