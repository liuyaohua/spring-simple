package com.example.liuyaohua.model;

import java.io.Serializable;
import java.util.Date;

/**
 * 作者 yaohua.liu
 * 日期 2016-09-05 16:51
 * 说明 token，用于数据加密
 */
public class Token extends BaseInfo implements Serializable {
	public Token() {
	}

	public Token(BaseInfo baseInfo) {
		super.setUserId(baseInfo.getUserId());
		super.setPhoneNumber(baseInfo.getPhoneNumber());
		super.setClientIdentify(baseInfo.getClientIdentify());
		super.setSystemPhone(baseInfo.getSystemPhone());
		super.setSystemModel(baseInfo.getSystemModel());
		super.setPlatformCode(baseInfo.getPlatformCode());
		super.setLng(baseInfo.getLng());
		super.setLat(baseInfo.getLat());
		super.setName(baseInfo.getName());
	}

	/**
	 * 创建时间 单位：毫秒
	 */
	long createTime = new Date().getTime();
	/**
	 * 有效期 单位：毫秒
	 */
	//long periodOfValidit = 7 * 24 * 60 * 60 * 1000;
	long periodOfValidit = 7 * 24 * 60 * 60 * 1000;

	public long getCreateTime() {
		return createTime;
	}

	public void setCreateTime(long createTime) {
		this.createTime = createTime;
	}

	public long getPeriodOfValidit() {
		return periodOfValidit;
	}

	public void setPeriodOfValidit(long periodOfValidit) {
		this.periodOfValidit = periodOfValidit;
	}



	@Override
	public String toString() {
		return "Token{" +
				"createTime=" + createTime +
				", periodOfValidit=" + periodOfValidit +
				"}\nBaseInfo = " + super.toString();
	}
}
