package com.example.liuyaohua.guava.cache;

import com.google.common.cache.*;
import org.junit.Test;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * 作者 yaohua.liu
 * 日期 2014-11-25 16:32
 *
 * 说明 ...
 */
public class CacheLoaderTest {

    @Test
    public void testCache() throws ExecutionException {
        LoadingCache<Object, Object> cache = cached(
                new CacheLoader<Object, Object>() {
                    @Override
                    public Object load(Object key) throws Exception {// 缓存数据获取，如果缓存中已经存在该key对应的value，则不执行该方法获取数据
                        System.out.println(key + " from db!");
                        return key + " from db!";
                    }
                });
        Object value=cache.apply("peida"); // 添加 key
        cache.get("peida");
        cache.get("peida2");
        long size=cache.size();
        cache.invalidate("peida");// 删除key
    }

    /**
     * 不需要延迟处理(泛型的方式封装)
     *
     * @return
     */
    public <K, V> LoadingCache<K, V> cached(CacheLoader<K, V> cacheLoader) {
        LoadingCache<K, V> cache = CacheBuilder
                .newBuilder()
                .maximumSize(10000)
                .weakKeys()
                .softValues()
                .refreshAfterWrite(120, TimeUnit.SECONDS)
                .expireAfterWrite(10, TimeUnit.MINUTES)
                .removalListener(// 添加Entry移除监听器
                        new RemovalListener<K, V>() {
                                public void onRemoval(RemovalNotification<K, V> rn) {
                                System.out.println(rn.getKey() + " 被移除");
                            }
                        }
                )
                .build(cacheLoader);
        return cache;
    }
}
