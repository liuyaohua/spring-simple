package com.example.liuyaohua.cache;

import com.google.common.cache.CacheLoader;
import com.google.common.collect.ImmutableMap;
import org.junit.*;
import org.junit.rules.ExpectedException;

import java.util.*;
import java.util.concurrent.ExecutionException;

/**
 * <p>作者 yaohua.liu
 * <p>日期 2018-06-25 22:38
 * <p>说明 ...
 */
public class AbstractGuavaCacheTest {
    private int key = 2;
    private String value = "vv";
    private TestGuavaCache cache = new TestGuavaCache();

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp() throws Exception {
        cache = new TestGuavaCache();
    }

    @After
    public void tearDown() throws Exception {
        cache = null;
    }

    @Test
    public void getNull() throws ExecutionException {
        // 测试获取空值情况
        try {
            cache.get(0);
        } catch (Exception e) {
            // 第一次返回空
            Assert.assertTrue(e instanceof CacheLoader.InvalidCacheLoadException);
        }
        // 第二次同样返回空
        thrown.expect(CacheLoader.InvalidCacheLoadException.class);
        cache.get(0);
    }

    @Test
    public void getFromCache() throws ExecutionException {
        // 测试空值
        String v = cache.getFromCache(key - 1);
        Assert.assertNull(v);

        // 测试默认值
        v = cache.getFromCache(key);
        Assert.assertNotNull(v);
        Assert.assertEquals("b", v);

        // 测试Guava缓存里面没有产生Null值
        thrown.expect(CacheLoader.InvalidCacheLoadException.class);
        cache.get(key - 1);
    }

    @Test
    public void getUnchecked() {
        // 测试默认值
        String v = cache.getUnchecked(key);
        Assert.assertNotNull(v);
        Assert.assertEquals("b", v);

        // 测试指定一个值
        cache.put(key, value);
        v = cache.getUnchecked(key);
        Assert.assertNotNull(v);
        Assert.assertEquals(value, v);
    }

    @Test
    public void getAll() {
        cache.put(5, "55");
        Set<Integer> set = new HashSet<>();
        set.add(5);
        set.add(6);
        // 测试一个默认值("b")，一个指定值("55")
        Map<Integer, String> map = cache.getAll(set);
        Assert.assertNotNull(map);
        Assert.assertEquals(2, map.size());
        Assert.assertNotNull(map.get(5));
        Assert.assertEquals("55", map.get(5));
        Assert.assertEquals("b", map.get(6));

        // 测试查询一个默认值("b")
        set.add(7);
        map = cache.getAll(set);
        Assert.assertNotNull(map);
        Assert.assertEquals(3, map.size());
        Assert.assertEquals("b", map.get(7));

        // 测试put一个值
        cache.put(8, value);
        set.add(8);
        map = cache.getAll(set);
        Assert.assertNotNull(map);
        Assert.assertEquals(4, map.size());
        Assert.assertEquals(value, map.get(8));
    }

    @Test
    public void getAllForList() {
        cache.put(5, "55");
        Set<Integer> set = new HashSet<>();
        set.add(5);
        set.add(6);
        // 测试一个默认值("b")，一个指定值("55")
        Collection<String> collection = cache.getAllForList(set);
        Assert.assertNotNull(collection);
        Assert.assertEquals(2, collection.size());

        // 测试put一个值
        cache.put(8, value);
        set.add(8);
        collection = cache.getAllForList(set);
        Assert.assertNotNull(collection);
        Assert.assertEquals(3, collection.size());
    }
}