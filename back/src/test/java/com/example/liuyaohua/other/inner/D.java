package com.example.liuyaohua.other.inner;

/**
 * 作者 yaohua.liu
 * 日期 2015-09-07 19:09
 *
 * 说明 ...
 */
public class D {
    abstract class T1{}
    abstract class T2{}
    public static void main(String[] args) {
        D d =new D();
        Many many = d.getMany();
    }

    public Many getMany(){
        return new Many();
    }

     class Many extends T1 implements Foo,Vehicle {

         @Override
         public void f() {

         }

         @Override
         public void drive() {

         }
     }
}
