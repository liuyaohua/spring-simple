package com.example.liuyaohua.guava.collect;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.Multimap;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Collection;
import java.util.Map;

/**
 * 作者 yaohua.liu
 * 日期 2014-11-06 18:19
 * * 说明 ...
 */
public class MultimapTest {

    @Test
    public void testHashMultimap() {
        Multimap<String, String> multimap = HashMultimap.create();
        multimap.put("a", "b");
        multimap.put("a", "b"); //
        multimap.put("a", "c");
        multimap.put("b", null);

        // 每增加一个 Entry 只要不与原来的Entry(key与value)完全重复，size都会+1
        int size=multimap.size();
        Assert.assertEquals(3,size);

        // get(key) 返回一个集合
        Collection<String> get = multimap.get("a");
        Assert.assertNotNull(get);
        Assert.assertEquals(get.size(),2);

        // 判断是否为空
        boolean isEmpty=multimap.isEmpty();
        Assert.assertTrue(!isEmpty);

        // 是否存在实体（key与value都相同）
        boolean existEntry = multimap.containsEntry("a", "b");
        boolean existKey = multimap.containsKey("a");
        boolean existValue = multimap.containsValue("b");

        // 把Multimap转换成Map
        Map<String, Collection<String>> map = multimap.asMap();
    }

    @Test
    public void testLinkedHashMultimap(){
        Multimap<String,String> multimap= LinkedHashMultimap.create();
        multimap.put("a","b");
    }
}
