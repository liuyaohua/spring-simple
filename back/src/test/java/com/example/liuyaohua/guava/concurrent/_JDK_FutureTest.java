package com.example.liuyaohua.guava.concurrent;

import org.junit.Test;

import java.util.concurrent.*;

/**
 * 作者 yaohua.liu
 * 日期 2014-11-26 17:37
 * * 说明 ...
 */
public class _JDK_FutureTest {

    @Test
    public void testFuture() {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        Future<String> future=executor.submit(
                new Callable<String>() {//使用Callable接口作为构造参数
                    public String call() {
                        //真正的任务在这里执行，这里的返回值类型为String，可以为任意类型
                        return "xx";
                    }
                });
        //在这里可以做别的任何事情
        //同上面取得结果的代码
        String result = "";
        //在这里可以做别的任何事情
        try {
            //取得结果，同时设置超时执行时间为5秒。同样可以用future.get()，不设置执行超时时间取得结果
            result = future.get(5000, TimeUnit.MILLISECONDS);
            System.out.println(result);
        } catch (InterruptedException e) {
            future.cancel(true);
        } catch (ExecutionException e) {
            future.cancel(true);
        } catch (TimeoutException e) {
            future.cancel(true);
        } finally {
            executor.shutdown();
        }
    }
}
