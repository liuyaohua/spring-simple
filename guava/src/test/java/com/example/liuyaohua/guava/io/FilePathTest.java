package com.example.liuyaohua.guava.io;

import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.net.URL;

/**
 * <p>作者 yaohua.liu
 * <p>日期 2018-11-15 15:06
 * <p>说明 Java获取文件路径的几种方法
 */
public class FilePathTest {

    @Test
    public void testPath() {
        // 获取当前类的所在工程的路径
        File f = new File(this.getClass().getResource("/").getPath());
        System.out.println(f.getAbsolutePath());

        // 获取当前类的路径
        f = new File(this.getClass().getResource("").getPath());
        System.out.println(f.getAbsolutePath());
    }

    @Test
    public void testPath2() throws IOException {
        // 获取当前类的所在工程路径;
        File directory = new File("");//参数为空
        String courseFile = directory.getCanonicalPath();
        System.out.println(courseFile);
    }

    @Test
    public void testPath3() throws IOException {
        // 获取当前工程src/resources目录下from.txt文件的路径
        URL xmlpath = this.getClass().getClassLoader().getResource("from.txt");
        System.out.println(xmlpath);
    }

    @Test
    public void testPath4() {
        // 获取当前工程路径
        String path = System.getProperty("user.dir");
        System.out.println(path);
    }

    @Test
    public void testPath5() {
        // 获取当前工程路径
        String path = System.getProperty("java.class.path");
        System.out.println(path);
    }
}
