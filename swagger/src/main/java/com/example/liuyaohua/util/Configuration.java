package com.example.liuyaohua.util;

import com.example.liuyaohua.model.enums.ChannelType;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.example.liuyaohua.model.enums.ChannelType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ResourceBundle;

/**
 * 作者 yaohua.liu
 * 日期 2016-08-10 11:38
 * 说明 配置文件读取工具类
 */
public class Configuration {
    private static Logger LOG = LoggerFactory.getLogger(Configuration.class);

    /**
     * 根据 key 从配置文件 properties/common.properties 中查找value
     *
     * @param key 需要查找的key
     * @return
     */
    public static String getValue(String key) {
        return getValue(key, ChannelType.COMMON);
    }

    /**
     * 根据 key 及文件名从配置文件中查找value
     *
     * @param key         需要查找的key
     * @param channelType 渠道类型
     * @return
     */
    public static String getValue(String key, ChannelType channelType) {
        Preconditions.checkNotNull(key, "查询的key不能为空！");
        Preconditions.checkNotNull(channelType, "渠道类型不能为空！");

        String fileName = channelType.getPropertiesFilePath();

        try {
            ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle(fileName);
            return RESOURCE_BUNDLE.getString(key);
        } catch (Exception e) {
            LOG.error("不能在配置文件{}中发现参数：!{}! message = ", fileName, key, e.getMessage(), e);
            throw new RuntimeException("不能在配置文件" + fileName + "中发现参数：" + '!' + key + '!');
        }
    }

    public static int getIntValue(String key) {
        return getIntValue(key, ChannelType.COMMON);
    }

    public static int getIntValue(String key, ChannelType channelType) {
        String value = getValue(key, channelType);
        if (!Strings.isNullOrEmpty(value) && !"".equals(value.trim())) {
            return Integer.parseInt(value);
        }
        return 0;
    }
}
