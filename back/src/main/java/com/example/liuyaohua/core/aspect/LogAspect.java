package com.example.liuyaohua.core.aspect;

import com.alibaba.fastjson.JSON;
import com.example.liuyaohua.core.exception.ServiceException;
import com.example.liuyaohua.model.Token;
import com.example.liuyaohua.model.enums.ErrorCode;
import com.google.common.base.Strings;
import com.example.liuyaohua.core.exception.ServiceException;
import com.example.liuyaohua.util.ResponseUtil;
import com.example.liuyaohua.model.Token;
import com.example.liuyaohua.model.enums.ErrorCode;
import com.example.liuyaohua.util.Configuration;
import com.example.liuyaohua.util.FilterUtils;
import com.example.liuyaohua.util.TokenUtil;
import org.aspectj.lang.ProceedingJoinPoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * 作者 yaohua.liu
 * 日期 2016-11-29 11:32
 * 说明 日志aop
 */
public class LogAspect {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	/**
	 * token加密key（重要）
	 */
	private String TOKEN_KEY = Configuration.getValue("TOKEN_KEY");

	private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	public Object doAround(ProceedingJoinPoint pjp) throws Throwable {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();

		Token token = TokenUtil.getTokenFromRequest(request, TOKEN_KEY);

		if(token == null || Strings.isNullOrEmpty(token.getPlatformCode()) || Strings.isNullOrEmpty(token.getUserId())){
			token = getParamsFromRequest(request);
		}

		long currentTime = System.currentTimeMillis();
		try {
			ResponseUtil responseUtil;
			MDC.put("PLATFORM_CODE", token.getPlatformCode());
			MDC.put("USER_ID", token.getUserId());
			logger.info("request_start:[time={}],[url={}]", sdf.format(new Date(currentTime)), FilterUtils.getUrlInfo(request));
			Object result = pjp.proceed();
			long endTime = System.currentTimeMillis();
			if (result instanceof ResponseUtil) {
				responseUtil = (ResponseUtil) result;
				logger.info("request_end:[time={}],[url={}],[times={}ms],response_info={}", sdf.format(new Date(endTime)), request.getRequestURL(), endTime - currentTime, JSON.toJSONString(responseUtil));
			}
			return result;
		} catch (Throwable exception) {
			long endTime = System.currentTimeMillis();
			if(exception instanceof ServiceException){
				ServiceException serviceException = (ServiceException) exception;
				ResponseUtil errorResult = ResponseUtil.returnFail(serviceException.getCode(),serviceException.getMessage());
				if(serviceException.isPrintStackInfo()){// 需要打印栈信息(默认)
					logger.error("request_error:[time={}],[url={}],[times={}ms],response_info={}", sdf.format(new Date(endTime)), request.getRequestURL(), endTime - currentTime, JSON.toJSONString(errorResult), exception);
				}else { // 业务不需要打印栈信息
					logger.error("request_error:[time={}],[url={}],[times={}ms],response_info={}", sdf.format(new Date(endTime)), request.getRequestURL(), endTime - currentTime, JSON.toJSONString(errorResult));
				}
				return errorResult;
			}
			logger.info("request_error:[time={}],[url={}],[times={}ms],response_info={}", sdf.format(new Date(endTime)), request.getRequestURL(), endTime - currentTime, "code="+ErrorCode.SERVICE_SERVER_UNKNOW_ERROR.getCode()+",msg="+exception.getMessage(), exception);
			return ResponseUtil.returnFail(ErrorCode.SERVICE_SERVER_UNKNOW_ERROR);
		}finally {
			try {
				MDC.clear();
			}catch (Exception e){
				logger.error("清空MDC出错！");
			}

		}
	}

	/**
	 * 从request中获取基本信息（userId & platformCode），各参数不存在都返回N
	 *
	 * @param request 请求
	 * @return
	 */
	private Token getParamsFromRequest(HttpServletRequest request) {
		Token baseInfo = new Token();
		Map<String, String[]> params = request.getParameterMap();
		baseInfo.setUserId(getIndex0(params.get("userId")));
		baseInfo.setPlatformCode(getIndex0(params.get("platformCode")));
		return baseInfo;
	}

	/**
	 * 从数组中获取第一个元素
	 *
	 * @param array 数组
	 * @return 异常则返回“N”
	 */
	private String getIndex0(String[] array) {
		if (array == null || array.length == 0) {
			return "N";
		}
		if (Strings.isNullOrEmpty(array[0])) {
			return "N";
		}
		return array[0];
	}

}
