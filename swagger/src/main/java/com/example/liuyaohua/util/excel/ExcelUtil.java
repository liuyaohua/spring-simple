package com.example.liuyaohua.util.excel;



import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * 作者 yaohua.liu
 * 日期 2016-08-25 15:06
 * 说明 ...
 */
public class ExcelUtil {

    public static String [][] getDataFromExcel(File file) throws IOException {
        InputStream in = null;
        Workbook workbook = null;
        try {
            in = new FileInputStream(file);
            workbook = new HSSFWorkbook(in);
        } catch (Exception e) {
            try {
                workbook = new XSSFWorkbook(in);
            } catch (Exception exception) {
                System.out.println(exception.getMessage());
            }
        } finally {
            if (null != in) {
                try {
                    in.close();
                } catch (IOException e) {

                }
            }
        }

        Sheet sheet = workbook.getSheetAt(0);
        int totalRow = sheet.getLastRowNum();
        int cellSize = sheet.getRow(0).getLastCellNum();
        String [][] ss = new String[totalRow+1][cellSize];
        for (int i = 0; i <= totalRow; i++) {
            Row row = sheet.getRow(i);
            for(int j=0;j<cellSize;j++){
                ss[i][j] = getCellValue(row.getCell(j));
            }
        }

        return ss;
    }

    private static String getCellValue(Cell cell) {
        String cellValue = "";
        DataFormatter formatter = new DataFormatter();
        if (cell != null) {
            switch (cell.getCellType()) {
            case Cell.CELL_TYPE_NUMERIC:
//                if (DateUtil.isCellDateFormatted(cell)) {
                    cellValue = formatter.formatCellValue(cell);
//                } else {
//                    double value = cell.getNumericCellValue();
//                    int intValue = (int) value;
//                    cellValue = value - intValue == 0 ? String.valueOf(intValue) : String.valueOf(value);
//                }
                break;
            case Cell.CELL_TYPE_STRING:
                cellValue = cell.getStringCellValue();
                break;
            case Cell.CELL_TYPE_BOOLEAN:
                cellValue = String.valueOf(cell.getBooleanCellValue());
                break;
            case Cell.CELL_TYPE_FORMULA:
                cellValue = String.valueOf(cell.getCellFormula());
                break;
            case Cell.CELL_TYPE_BLANK:
                cellValue = "";
                break;
            case Cell.CELL_TYPE_ERROR:
                cellValue = "";
                break;
            default:
                cellValue = cell.toString().trim();
                break;
            }
        }
        return cellValue.trim();
    }
}