package com.example.liuyaohua.guava.collect;

import com.google.common.base.Function;
import com.google.common.base.Functions;
import com.google.common.collect.ComparisonChain;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Ordering;
import org.junit.Test;

import java.util.Comparator;
import java.util.List;

/**
 * 作者 yaohua.liu
 * 日期 2014-09-18 17:09
 * * 说明 ...
 */
public class OrderingTest {

    Comparator<String> comparator = new Comparator<String>() {
        @Override
        public int compare(String o1, String o2) {
            return o1.compareTo(o2);
        }
    };

    /**
     * 给字符串列表排序
     */
    @Test
    public void testStringList() {

        // String.CASE_INSENSITIVE_ORDER
        // A Comparator that orders String objects as by compareToIgnoreCase
        Ordering<Object> ordering = Ordering.from(String.CASE_INSENSITIVE_ORDER).onResultOf(Functions.toStringFunction());


        List<String> list = ImmutableList.of("3", "1", "2");

        List<String> sort = ordering.sortedCopy(list);
        String result = sort.toString();

        // 查询是否有序
        Boolean b = Ordering.from(comparator).reverse().isOrdered(list);
    }

    /**
     * 给自定义类对象排序
     */
    @Test
    public void testOrdering() {
        Ordering<Foo> ordering = Ordering
                .natural()// 自然排序
                .nullsFirst()// 空的放前面
                .reverse()// 反转排序结果
                .onResultOf(
                        new Function<Foo, String>() {
                            public String apply(Foo foo) {
                                return foo.sortedBy;
                            }
                        });

        List<Foo> foosList = ImmutableList.of(new Foo("a", 2),new Foo("b", 2),new Foo("a", 1));
        // 传统的排序方法
       // Collections.sort(foosList);

        // 使用Ordering排序
        List<Foo> sort = ordering.sortedCopy(foosList);

        String order = "";
        for (Foo foo : foosList) {
            order += foo.sortedBy + ":" + foo.notSortedBy + ";";
        }
        System.out.println(order);
    }

    class Foo implements Comparable<Foo> {
        String sortedBy;
        int notSortedBy;

        public Foo(String sortedBy, int notSortedBy) {
            this.sortedBy = sortedBy;
            this.notSortedBy = notSortedBy;
        }

        /**
         * jdk 方式的比较
         *
         * @return
         */
        /*@Override
                    public int compareTo(Foo other) {
                        int cmp = sortedBy.compareTo(other.sortedBy);
                        if (cmp != 0) {
                            return cmp;
                        } else {
                            return notSortedBy - other.notSortedBy;
                        }
                    }*/

        /**
         * Guava的比较方式
         *
         * @param that
         * @return
         */
        public int compareTo(Foo that) {
            return ComparisonChain.start()
                    .compare(this.sortedBy, that.sortedBy, Ordering.natural().nullsFirst())
                    .compare(this.notSortedBy, that.notSortedBy)//二级排序
                    .result();
        }
    }
}
