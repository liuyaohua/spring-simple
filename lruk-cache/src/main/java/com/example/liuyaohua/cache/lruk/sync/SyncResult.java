package com.example.liuyaohua.cache.lruk.sync;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import java.io.Serializable;
import java.util.List;

/**
 * <p>作者 yaohua.liu
 * <p>日期 2018-04-18 15:07
 * <p>说明 ...
 */
public class SyncResult implements Serializable {
    private int succeedCount = 0;
    private int failedCount = 0;
    private int allCount = 0;

    private List<SyncDetail> syncDetails = Lists.newArrayList();

    public void addResult(APIResponse<Boolean> r, String url, long time, int retryTimes) {
        if (Strings.isNullOrEmpty(url)) {
            return;
        }
        allCount++;
        syncDetails.add(new SyncDetail(url, time, retryTimes));
        if (r == null || r.getData() == null || !r.getData()) {
            failedCount++;
            return;
        }
        succeedCount++;
    }

    public int getSucceedCount() {
        return succeedCount;
    }

    public void setSucceedCount(int succeedCount) {
        this.succeedCount = succeedCount;
    }

    public int getFailedCount() {
        return failedCount;
    }

    public void setFailedCount(int failedCount) {
        this.failedCount = failedCount;
    }

    public int getAllCount() {
        return allCount;
    }

    public void setAllCount(int allCount) {
        this.allCount = allCount;
    }

    public List<SyncDetail> getSyncDetails() {
        return syncDetails;
    }

    public void setSyncDetails(List<SyncDetail> syncDetails) {
        this.syncDetails = syncDetails;
    }

    public class SyncDetail implements Serializable {
        public SyncDetail() {
        }

        public SyncDetail(String address) {
            this.address = address;
        }

        public SyncDetail(String address, long syncTime, int retryTimes) {
            this.address = address;
            this.syncTime = syncTime;
            this.retryTimes = retryTimes;
        }

        /**
         * 需要同步的ip
         */
        String address = "";
        /**
         * 同步时所花时间
         */
        long syncTime = 0;
        /**
         * 重试次数
         */
        int retryTimes = 0;

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public long getSyncTime() {
            return syncTime;
        }

        public void setSyncTime(long syncTime) {
            this.syncTime = syncTime;
        }

        public int getRetryTimes() {
            return retryTimes;
        }

        public void setRetryTimes(int retryTimes) {
            this.retryTimes = retryTimes;
        }

        @Override
        public String toString() {
            return "SyncDetail{" +
                    "address='" + address + '\'' +
                    ", syncTime=" + syncTime +
                    ", retryTimes=" + retryTimes +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "SyncResult{" +
                "succeedCount=" + succeedCount +
                ", failedCount=" + failedCount +
                ", allCount=" + allCount +
                ", syncDetails=" + syncDetails +
                '}';
    }
}
