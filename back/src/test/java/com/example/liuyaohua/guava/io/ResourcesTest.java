package com.example.liuyaohua.guava.io;

import com.google.common.base.CharMatcher;
import com.google.common.base.Charsets;
import com.google.common.io.LineProcessor;
import com.google.common.io.Resources;
import org.junit.Test;

import java.io.IOException;
import java.net.URL;
import java.util.List;

/**
 * 作者 yaohua.liu
 * 日期 2014-09-22 11:37
 * * 说明 Provides utility methods for working with resources in the classpath
 */
public class ResourcesTest {

    @Test
    public void testResources() throws IOException {
        // Resources.toString 获取本地文件
        URL url=getClass().getClassLoader().getResource("from.txt");
        String content = Resources.toString(url, Charsets.UTF_8);

        // Resources.readLines
        List<String> lines=Resources.readLines(url, Charsets.UTF_8);

        URL url2 = Resources.getResource(ResourcesTest.class, "ResourcesTest.class");
        Resources.readLines(url2, Charsets.UTF_8, new LineProcessor<Object>() {
            @Override
            public boolean processLine(String line) throws IOException {
                System.out.println(CharMatcher.JAVA_LETTER.retainFrom(line));
                return true;
            }

            @Override
            public Object getResult() {
                return null;
            }
        });

    }
}
