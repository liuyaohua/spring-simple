package com.example.liuyaohua.cache.lruk.k;

import com.example.liuyaohua.cache.lruk.base.Ticker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * 作者 yaohua.liu
 * 日期 2018-03-20 16:17
 * 说明 K值数据结构
 */
public class KRadix {
    private final Logger logger = LoggerFactory.getLogger(KRadix.class);
    private static ExecutorService executor = new ThreadPoolExecutor(1, 10, 30L, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>(100000));
    /**
     * Measures time in a testable way.
     */
    private final Ticker ticker;
    /**
     * 访问后多长时间重设K值 30秒=30 * 1000 000 纳秒
     */
    private final long expireAfterAccessNanos = TimeUnit.SECONDS.toNanos(30);

    private static final int UNSET_INT = 0;
    /**
     * 修改后多长时间重设K值
     */
    private final long expireAfterWriteNanos = UNSET_INT;

    /**
     * 最大行大小
     */
    private final static int MAX_ROW_SIZE = 1 << 10;
    /**
     * 最大列大小
     */
    private final static int MAX_COLUMN_SIZE = 1 << 12;

    /**
     * 权重数据
     */
    private kUnit[][] data = null;

    private final int kRowShift;
    private final int kRowMask;

    private final int kRowSize;
    private final int kColumnSize;

    private final int kColumnShift;
    private final int kColumnMask;

    public KRadix(int rowSize, int columnSize, Ticker ticker, long expireAfterAccessNanos, long expireAfterWriteNanos) {
        this.ticker = ticker;
        //this.expireAfterAccessNanos = expireAfterAccessNanos;
        //this.expireAfterWriteNanos = expireAfterWriteNanos;
        if (rowSize <= 0) {
            throw new IllegalArgumentException("行大小不能小于等于0！");
        }
        if (columnSize <= 0) {
            throw new IllegalArgumentException("列大小不能小于等于0！");
        }
        rowSize = Math.min(rowSize, MAX_ROW_SIZE);
        columnSize = Math.min(columnSize, MAX_COLUMN_SIZE);

        int kRowShift = 0;
        int kRowSize = 1;
        while (kRowSize < rowSize) {
            ++kRowShift;
            kRowSize <<= 1;
        }
        this.kRowShift = 32 - kRowShift;
        kRowMask = kRowSize - 1;

        int kColumnShift = 0;
        int kColumnSize = 1;
        while (kColumnSize < columnSize) {
            ++kColumnShift;
            kColumnSize <<= 1;
        }
        this.kColumnShift = 32 - kColumnShift;
        this.kColumnMask = kColumnSize - 1;

        this.kRowSize = kRowSize;
        this.kColumnSize = kColumnSize;
        this.data = new kUnit[kRowSize][kColumnSize];
        for (int i = 0; i < kRowSize; i++) {
            data[i] = new kUnit[kColumnSize];
            for (int j = 0; j < kColumnSize; j++) {
                data[i][j] = new kUnit(i, j);
                //logger.info("创建lruk统计数据单元成功，row = {}, column = {}", i, j);
            }
        }
    }

    public kUnit[][] getData() {
        return data;
    }

    /**
     * 根据code获取权重数据
     *
     * @param hash 整数值
     * @return com.tujia.tns.baseinfo.api.util.lruk.k.Weight
     */
    public kUnit getWeightByCode(int hash) {
        return getWeightByCode(hash, false);
    }

    /**
     * 根据code获取权重数据，如果没有获取到则异步创建并立即返回null
     *
     * @param hash             整数值
     * @param updateAccessTime 是否更新上次访问时间
     * @return {@link kUnit}
     */
    public kUnit getWeightByCode(int hash, boolean updateAccessTime) {
        int row = (hash >>> kRowShift) & kRowMask;
        int column = hash & kColumnMask;
        kUnit kUnit = data[row][column];
        if (kUnit == null) {
            executor.submit(() -> createWeigher(data, row, column));
            return null;
        }
        return updateAccessTime ? recordsKUnit(kUnit) : kUnit;
    }

    boolean expires() {
        return expiresAfterWrite() || expiresAfterAccess();
    }

    boolean expiresAfterWrite() {
        return expireAfterWriteNanos > 0;
    }

    boolean expiresAfterAccess() {
        return expireAfterAccessNanos > 0;
    }

    /**
     * 判断是否读或写过期
     *
     * @param unit kUnit
     * @param now  时间
     * @return
     */
    boolean isExpired(kUnit unit, long now) {
        checkNotNull(unit);
        if (expiresAfterAccess() && (now - unit.getAccessTime() >= expireAfterAccessNanos)) {
            return true;
        }
        if (expiresAfterWrite() && (now - unit.getWriteTime() >= expireAfterWriteNanos)) {
            return true;
        }
        return false;
    }

    /**
     * 记录上一次访问时间，过期则更新权重
     *
     * @param unit kUnit
     * @return
     */
    private kUnit recordsKUnit(kUnit unit) {
        if (unit == null) {
            throw new IllegalStateException();
        }
        if (isExpired(unit, ticker.read())) {//过期了
            unit.setWeight(1);
        } else {
            unit.addWeight();
        }
        unit.setAccessTime(ticker.read());
        return unit;
    }

    /**
     * 创建权重
     *
     * @param data   kUnit[][]
     * @param row    行位移
     * @param column 列位移
     * @return
     */
    private kUnit createWeigher(kUnit[][] data, int row, int column) {
        if (data == null) {
            return null;
        }
        kUnit kUnit = data[row][column];
        if (kUnit != null) {
            return kUnit;
        }
        try {
            kUnit = new kUnit();
            kUnit.setAccessTime(ticker == null ? Ticker.systemTicker().read() : ticker.read());
            kUnit.setWriteTime(ticker == null ? Ticker.systemTicker().read() : ticker.read());
            kUnit.setWeight(1);
            data[row][column] = kUnit;
            logger.info("创建lruk统计数据单元成功，row = {}, column = {}", row, column);
            return kUnit;
        } catch (Exception e) {
            logger.info("创建lruk统计数据单元失败，error msg = {}", e.getMessage(), e);
            return null;
        }
    }

    public int getkRowShift() {
        return kRowShift;
    }

    public int getkRowMask() {
        return kRowMask;
    }
}
