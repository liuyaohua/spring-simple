package com.example.liuyaohua.other.inner;

/**
 * 作者 yaohua.liu
 * 日期 2015-09-07 15:39
 *
 * 说明 ...
 */
public class TestCommon {
    class Inner {
        void print(){
            System.out.println("ccc");
        }
    }

    Inner getInner(){
        return new Inner();
    }

    public static void main(String[] args) {
        TestCommon testCommon =new TestCommon();
        Inner cc = testCommon.getInner();
        cc.print();
    }

}
