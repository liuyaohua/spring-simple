package com.example.liuyaohua.controller;

import com.example.liuyaohua.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;

/**
 * 抽象controller
 *
 * @author yaohua.liu Date: 2015年6月1日 下午5:24:06
 * @version $Id
 */
public abstract class AbstractController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * 获得登录用户
     * @param request
     */
/*    public String getLoginUserName(HttpServletRequest request) {
        //UserContext.getUserInfo().getUserName();
        CookieUser info = UserClient.getUser(request);
        if(info == null || info.getUserName() == null){
            return StringUtils.EMPTY;
        }
        return info.getUserName();
    }
    */

    /**
     * 处理所有controller 中的异常信息
     */
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public Object SystemExceptionHandler(Exception exception, WebRequest request) {
            /*暂不封装，打印业务异常就可以*/
        logger.error("system Exception: message = {}" , exception.getMessage() ,exception);
        return ResponseUtil.returnFail("-2",exception.getMessage()) /*APIResponse.returnFail(-2, exception.getMessage())*/;
    }
}
