package com.example.liuyaohua.cache.lruk.lru;

import com.example.liuyaohua.cache.lruk.lru.status.CacheStats;
import com.example.liuyaohua.cache.lruk.lru.status.LongAddable;
import com.example.liuyaohua.cache.lruk.lru.status.LongAddables;
import com.example.liuyaohua.cache.lruk.lru.status.CacheStats;
import com.example.liuyaohua.cache.lruk.lru.status.LongAddable;
import com.example.liuyaohua.cache.lruk.lru.status.LongAddables;

/**
 * A thread-safe {@link StatsCounter} implementation for use by {@link Cache} implementors.
 *
 * @since 10.0
 */
public class SimpleStatsCounter implements StatsCounter {
    private final LongAddable hitCount = LongAddables.create();
    private final LongAddable missCount = LongAddables.create();
    private final LongAddable loadSuccessCount = LongAddables.create();
    private final LongAddable loadExceptionCount = LongAddables.create();
    private final LongAddable totalLoadTime = LongAddables.create();
    private final LongAddable evictionCount = LongAddables.create();

    /**
     * Constructs an instance with all counts initialized to zero.
     */
    public SimpleStatsCounter() {

    }

    /**
     * @since 11.0
     */
    @Override
    public void recordHits(int count) {
        hitCount.add(count);
    }

    /**
     * @since 11.0
     */
    @Override
    public void recordMisses(int count) {
        missCount.add(count);
    }

    @Override
    public void recordLoadSuccess(long loadTime) {
        loadSuccessCount.increment();
        totalLoadTime.add(loadTime);
    }

    @Override
    public void recordLoadException(long loadTime) {
        loadExceptionCount.increment();
        totalLoadTime.add(loadTime);
    }

    @Override
    public void recordEviction() {
        evictionCount.increment();
    }

    @Override
    public CacheStats snapshot() {
        return new CacheStats(
                hitCount.sum(),
                missCount.sum(),
                loadSuccessCount.sum(),
                loadExceptionCount.sum(),
                totalLoadTime.sum(),
                evictionCount.sum());
    }

    @Override
    public void reset(long x) {
        hitCount.reset(x);
        missCount.reset(x);
        loadSuccessCount.reset(x);
        loadExceptionCount.reset(x);
        totalLoadTime.reset(x);
        evictionCount.reset(x);
    }

    /**
     * Increments all counters by the values in {@code other}.
     */
    public void incrementBy(StatsCounter other) {
        CacheStats otherStats = other.snapshot();
        hitCount.add(otherStats.hitCount());
        missCount.add(otherStats.missCount());
        loadSuccessCount.add(otherStats.loadSuccessCount());
        loadExceptionCount.add(otherStats.loadExceptionCount());
        totalLoadTime.add(otherStats.totalLoadTime());
        evictionCount.add(otherStats.evictionCount());
    }
}
