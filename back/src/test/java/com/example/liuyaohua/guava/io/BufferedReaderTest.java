package com.example.liuyaohua.guava.io;

import com.google.common.io.Closer;
import org.junit.Test;

import java.io.*;

/**
 * 作者 yaohua.liu
 * 日期 2015-01-15 18:11
 * * 说明 ...
 */
public class BufferedReaderTest {
    @Test
    public void testBufferedreader() {
        String path = this.getClass().getClassLoader().getResource("from.txt").getPath();
        File from = new File(this.getClass().getClassLoader().getResource("").getPath() );
        Closer closer = Closer.create();
        try {
            InputStream is = null;
            // 获取输入流
            //is = closer.register(new ByteArrayInputStream(strs.getBytes("utf-8")));
            is = closer.register(new FileInputStream(from));
            BufferedReader buffer = closer.register(new BufferedReader(new InputStreamReader(is,"utf-8")));
            String line="";
            while((line=buffer.readLine())!=null){
                System.out.println(line);
            }
        } catch (Exception e) {

        } finally {
            try {
                closer.close();
            } catch (IOException e) {
                System.out.println("关闭上传007对帐流失败!");
            }
        }
    }
}
