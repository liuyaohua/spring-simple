package com.example.liuyaohua.designpattern;

/**
 * 作者 yaohua.liu
 * 日期 2017-05-31 17:41
 * 说明 普通工厂模式，就是建立一个工厂类，对实现了同一接口的一些类进行实例的创建
 * <p/>
 * 工厂模式适合：凡是出现了大量的产品需要创建，并且具有共同的接口时，可以通过工厂方法模式进行创建。在以上的三种模式中，第一种如果传入的字符串有误，不能正确创建对象，第三种相对于第二种，不需要实例化工厂类，所以，大多数情况下，我们会选用第三种——静态工厂方法模式。
 */
public class D1FactoryMethod {

	public Sender produce(String type) {
		if ("mail".equals(type)) {
			return new MailSender();
		} else if ("sms".equals(type)) {
			return new SmsSender();
		} else {
			System.out.println("请输入正确的类型!");
			return null;
		}
	}

	public static void main(String[] args) {
		D1FactoryMethod factory = new D1FactoryMethod();
		Sender sender = factory.produce("sms");
		sender.Send();
	}
	private class MailSender implements Sender {
		@Override
		public void Send() {
			System.out.println("this is mailsender!");
		}
	}
	private class SmsSender implements Sender {
		@Override
		public void Send() {
			System.out.println("this is sms sender!");
		}
	}
}
