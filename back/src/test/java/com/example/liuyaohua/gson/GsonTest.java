package com.example.liuyaohua.gson;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.junit.Before;
import org.junit.Test;

/**
 * 作者 yaohua.liu
 * 日期 2016-06-12 14:03
 * 说明 ...
 */
public class GsonTest {
    People p;

    public void ttt() {

    }

    @Before
    public void init() {
        p = new People();
        p.setAge(20);
        p.setName("People");
        p.setSetName(true);
        p.setAddress("xxx");
    }

    @Test
    public void test1() {
        Gson gson = new Gson();
        System.out.println(gson.toJson(p));
    }

    @Test
    public void test2() {
        ExclusionStrategy excludeStrategy = new SetterExclusionStrategy();
        Gson gson1 = new GsonBuilder()
                .setExclusionStrategies(excludeStrategy)
                .create();

        String json1 = gson1.toJson(p);
        System.out.println(json1);

        People p1 = gson1.fromJson(json1, People.class);
        System.out.println(p1);
    }

    @Test
    public void test3() {
//        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
//        System.out.println(gson.toJson(p));

        Gson gson2 = new GsonBuilder().create();
        String fullJson = gson2.toJson(p);
        System.out.println("fullJson = " + fullJson);
        //System.out.println(gson.toJson(gson2.fromJson(fullJson, People.class)));
    }

}

class SetterExclusionStrategy implements ExclusionStrategy {
    public boolean shouldSkipClass(Class<?> clazz) {
        return false;
    }

    public boolean shouldSkipField(FieldAttributes f) {
        return f.getName().startsWith("set");
    }
}

class People {
    @Expose(serialize = false, deserialize = false)
    String name;
    @Expose
    @SerializedName("aaa")
    int age;
    boolean setName = false;
    @SerializedName("addressName")
    String address = "";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean getSetName() {
        return setName;
    }

    public void setSetName(boolean setName) {
        this.setName = setName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "name=" + name + " age=" + age + " setName=" + setName;
    }
}

