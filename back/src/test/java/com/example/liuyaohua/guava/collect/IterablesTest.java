package com.example.liuyaohua.guava.collect;

import com.google.common.base.Predicate;
import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;
import org.junit.Test;

/**
 * 作者 yaohua.liu
 * 日期 2014-11-10 17:01
 * * 说明 Iterables类主要用于Iterable的二次处理,很多方法都是调用Iterators进行处理
 * iterable支持foreach方法，iterator不支持
 */
public class IterablesTest {

    private Iterable iterable = Splitter.on(";").omitEmptyStrings()
            .trimResults().split("a;b;c;;;;;b");

    private Predicate predicate = new Predicate() {
        @Override
        public boolean apply(Object input) {
            return input.equals("b");
        }
    };

    @Test
    public void testIterables() {
        // size
        int size = Iterables.size(iterable);

        // foreach 方法 iterable支持foreach方法，iterator不支持
        for (Object item : iterable){System.out.println(item);}

        // 过滤器
        Iterable result = Iterables.filter(iterable, predicate);

        // 转化成Array对象
        String ayy[] = Iterables.toArray(iterable, String.class);

        // 查找某个对象出现的次数
        int count=Iterables.frequency(iterable,"b");

        // 是否存在某个对象
        boolean exist = Iterables.any(iterable, predicate);

        // 查找符合条件的对象
        Object item = Iterables.find(iterable, predicate);

        // 合并多个iterable，最多可支持4个参数
        Iterable concat = Iterables.concat(iterable, iterable);

        // getFirst getLast
        Object first = Iterables.getFirst(iterable, "NULL");
        Object last = Iterables.getLast(iterable, "NULL");
    }
}
