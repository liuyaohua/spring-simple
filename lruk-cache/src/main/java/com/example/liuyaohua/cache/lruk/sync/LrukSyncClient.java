package com.example.liuyaohua.cache.lruk.sync;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.example.liuyaohua.http.HttpClientUtil;
import com.example.liuyaohua.http.HttpResultVO;
import com.example.liuyaohua.http.IpAddressUtil;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import com.example.liuyaohua.http.HttpClientUtil;
import com.example.liuyaohua.http.HttpResultVO;
import com.example.liuyaohua.http.IpAddressUtil;
import org.apache.http.conn.ConnectTimeoutException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.SocketTimeoutException;
import java.util.List;
import java.util.Map;

/**
 * <p>作者 yaohua.liu
 * <p>日期 2018-04-18 14:44
 * <p>说明 lruk本地缓存同步工具类
 */
public class LrukSyncClient {
    private final static Splitter splitter = Splitter.on("\n").omitEmptyStrings().trimResults();
    private final static Logger LOGGER = LoggerFactory.getLogger(LrukSyncClient.class);
    public static final String DEFAULT_URL = "/lruk/sync.do";
    private static final String LOCAL_HOST_NAME = IpAddressUtil.getHostName();
    private static final String LOCAL_HOST_IP = IpAddressUtil.getIpAddress();

    /**
     * 向address(可能有多个url)中的url依次发送请求，统计返回数据
     *
     * @param addressStr url(多个时以逗号分隔)，如 xxx.com:8080,yyy.com:8080
     * @param key        需要同步的key
     * @param type       同步类型 {@link EnumLrukSyncType}
     * @return SyncResult
     */
    public static SyncResult syncLrukCache(String addressStr, long key, EnumLrukSyncType type) {
        if (Strings.isNullOrEmpty(addressStr) || key <= 0 || type == null) {
            LOGGER.warn("参数存在为空的情况，同步结束！");
            return null;
        }

        List<String> addressList = splitter.splitToList(addressStr);
        if (addressList.size() == 0) {
            LOGGER.warn("请求的url为空，同步结束！");
            return null;
        }
        Map<String, String> params = Maps.newHashMap();
        params.put("key", key + "");
        params.put("type", type.getCode() + "");
        HttpClientUtil httpClientUtil = HttpClientUtil.getHttpClient(50,200);
        SyncResult syncResults = new SyncResult();
        for (String address : addressList) {
            if (Strings.isNullOrEmpty(address)) {
                continue;
            }
            if (address.startsWith("#") || !address.startsWith("http")) {
                continue;
            }
            if (address.contains(LOCAL_HOST_NAME) || address.contains(LOCAL_HOST_IP)) {
                continue;
            }
            String url = address + DEFAULT_URL;
            long l1 = System.currentTimeMillis();
            try {
                HttpResultVO hrv = httpClientUtil.doGetDetail(url, params);
                long l2 = System.currentTimeMillis();
                if (hrv.getStatusCode() != 200) {
                    LOGGER.error("同步lruk缓存请求URL({})响应时间={} ms,返回状态异常,状态码={}, key = {}, type = {}", url, (l2 - l1), hrv.getStatusCode(), key, type.getDesc());
                    continue;
                }
                String json = hrv.getResponseBody();
                LOGGER.info("同步lruk缓存请求URL({})响应时间={} ms,状态码:{}, type = {}, 返回的结果:{}", url, (l2 - l1), hrv.getStatusCode(), type.getDesc(), json);
                APIResponse<Boolean> result = JSON.parseObject(json, new TypeReference<APIResponse<Boolean>>() {});
                syncResults.addResult(result, url, l2 - l1, 0);
            } catch (ConnectTimeoutException cte) {
                long l2 = System.currentTimeMillis();
                syncResults.addResult(null, url, l2 - l1, 0);
                LOGGER.error("同步lruk缓存请求URL({})响应时间={} ms,请求链接超时, key = {}, type = {},异常信息为：{}", url, (l2 - l1), key, type.getDesc(), cte.getMessage(), cte);
            } catch (SocketTimeoutException ste) {
                long l2 = System.currentTimeMillis();
                syncResults.addResult(null, url, l2 - l1, 0);
                LOGGER.error("同步lruk缓存请求URL({}响应时间={} ms,获取数据读取超时, key = {}, type = {},异常信息为：{}", url, (l2 - l1), key, type.getDesc(), ste.getMessage());
            } catch (Exception e) {
                long l2 = System.currentTimeMillis();
                syncResults.addResult(null, url, l2 - l1, 0);
                LOGGER.error("同步lruk缓存请求URL({})响应时间={} ms,发生非超时异常, key = {}, type = {},异常信息为：{}", url, (l2 - l1), key, type.getDesc(), e.getMessage(), e);
            }
        }

        return syncResults;
    }
}