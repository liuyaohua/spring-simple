package com.example.liuyaohua.cache.lruk.lru;

import com.example.liuyaohua.cache.lruk.lru.status.CacheStats;
import com.google.common.collect.ImmutableMap;
import com.example.liuyaohua.cache.lruk.k.HitRateCounter;
import com.example.liuyaohua.cache.lruk.k.KLoader;
import com.example.liuyaohua.cache.lruk.lru.status.CacheStats;

import java.io.Serializable;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentMap;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * 作者 yaohua.liu
 * 日期 2018-03-24 12:34
 * 说明 ...
 */
public class LocalManualCache<K, V> implements Cache<K, V>, Serializable {
    protected final LocalCache<K, V> localCache;

    public LocalManualCache(CacheBuilder<? super K, ? super V> builder, CacheLoader<? super K, V> loader, KLoader kLoader) {
        this(new LocalCache<K, V>(builder, loader, kLoader));
    }

    public LocalManualCache(LocalCache<K, V> localCache) {
        this.localCache = localCache;
    }

    // Cache methods

    @Override

    public V getIfPresent(Object key) {
        return localCache.getIfPresent(key);
    }

    @Override
    public V get(K key, final Callable<? extends V> valueLoader) throws Exception {
        checkNotNull(valueLoader);
        return localCache.get(
                key,
                new CacheLoader<Object, V>() {
                    @Override
                    public V load(Object key) throws Exception {
                        return valueLoader.call();
                    }
                });
    }

    @Override
    public ImmutableMap<K, V> getAllPresent(Iterable<?> keys) {
        return localCache.getAllPresent(keys);
    }

    @Override
    public void put(K key, V value) {
        localCache.put(key, value);
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {
        localCache.putAll(m);
    }

    @Override
    public void invalidate(Object key) {
        checkNotNull(key);
        localCache.remove(key);
    }

    @Override
    public void invalidateAll(Iterable<?> keys) {
        localCache.invalidateAll(keys);
    }

    @Override
    public void invalidateAll() {
        localCache.clear();
    }

    @Override
    public long size() {
        return localCache.longSize();
    }

    @Override
    public ConcurrentMap<K, V> asMap() {
        return localCache;
    }

    @Override
    public CacheStats stats() {
        SimpleStatsCounter aggregator = new SimpleStatsCounter();
        aggregator.incrementBy(localCache.globalStatsCounter);
        for (Segment<K, V> segment : localCache.segments) {
            aggregator.incrementBy(segment.statsCounter);
        }
        return aggregator.snapshot();
    }

    @Override
    public CacheStats kHitRateStats() {
        long now = localCache.ticker.read();
        HitRateCounter cacheStats = localCache.hitRateCounter;

        CacheStats stats =  cacheStats.snapshot();
        if (now - cacheStats.getLastRecordTime() > HitRateCounter.recordTimeOut) {
            cacheStats.setLastRecordTime(now);
            cacheStats.reset(0L);
        }

        return stats;
    }

    @Override
    public void cleanUp() {
        localCache.cleanUp();
    }

    // Serialization Support

    private static final long serialVersionUID = 1;

}
