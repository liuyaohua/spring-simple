package com.example.liuyaohua.Interview;

import com.example.liuyaohua.other.String;
import org.junit.Test;

import java.util.concurrent.*;

/**
 * 作者 yaohua.liu
 * 日期 2017-05-27 16:47
 * 说明 ...
 */
public class CallableRunableTest {
	public class CallableTest implements Callable<java.lang.String>{
		@Override
		public java.lang.String call() throws Exception {
			System.out.println("callable...");
			return "callable";
		}
	}

	public class RunnagleTest implements Runnable{
		@Override
		public void run() {
			System.out.println("run...");
		}
	}

	@Test
	public void test() throws InterruptedException, ExecutionException, TimeoutException {
		ExecutorService service = Executors.newFixedThreadPool(2);

		RunnagleTest runnagleTest = new RunnagleTest();
		CallableTest callableTest = new CallableTest();

		Thread runnable = new Thread(runnagleTest);
		runnable.start();
		//Thread tt = new Thread(callableTest); 错误 Thread类只支持Runnable.

		Future <java.lang.String>callFuture = service.submit(callableTest);
		java.lang.String c = callFuture.get(1000, TimeUnit.MICROSECONDS);
		System.out.println(c);

		Future runFuture =service.submit(runnable);
		String r = (String) runFuture.get(1000, TimeUnit.MICROSECONDS);
		System.out.println(r);

//		run...
//		callable...
//		callable
//		null
	}

}
