package com.example.liuyaohua.other.inner;

/**
 * 作者 yaohua.liu
 * 日期 2015-09-07 15:08
 *
 * 说明 C、参数式的匿名内部类。
 */
public class C {

    void doStuff(Foo f) {
        f.f();
    }

    public static void main(String[] args) {
        C b = new C();
        b.doStuff(new Foo() {
            public void f() {
                System.out.println("foofy");
            }
        });
    }
}
