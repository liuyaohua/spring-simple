package com.example.liuyaohua.core;

import com.example.liuyaohua.core.annotation.BaseInfoType;
import com.example.liuyaohua.model.BaseInfo;
import com.example.liuyaohua.util.Configuration;
import com.example.liuyaohua.util.TokenUtil;
import com.example.liuyaohua.core.annotation.BaseInfoType;
import com.example.liuyaohua.model.BaseInfo;
import com.example.liuyaohua.util.Configuration;
import com.example.liuyaohua.util.TokenUtil;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import javax.servlet.http.HttpServletRequest;

/**
 * 作者 yaohua.liu
 * 日期 2016-11-02 17:45
 * 说明 基本信息解析器
 */
public class BaseInfoResolver implements HandlerMethodArgumentResolver {
	/**
	 * token加密key（重要）
	 */
	private String TOKEN_KEY = Configuration.getValue("TOKEN_KEY");
	/**
	 * 检查解析器是否支持解析该参数
	 */
	@Override
	public boolean supportsParameter(MethodParameter parameter) {
		return parameter.getParameterAnnotation(BaseInfoType.class) != null && parameter.getParameterType() == BaseInfo.class;
	}

	@Override
	public Object resolveArgument(MethodParameter parameter,
								  ModelAndViewContainer mavContainer, NativeWebRequest webRequest,
								  WebDataBinderFactory binderFactory) throws Exception {
		HttpServletRequest request= (HttpServletRequest) webRequest.getNativeRequest();
		return TokenUtil.getTokenFromRequest(request, TOKEN_KEY);
	}
}
