package com.example.liuyaohua.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 作者 yaohua.liu
 * 日期 2016-12-13 14:52
 * 说明 系统停服配置信息
 */
@ApiModel(value = "SystemStopServiceProfile", description = "系统停服配置信息")
public class SystemStopServiceProfile implements java.io.Serializable {
	@ApiModelProperty(hidden = true)
	int id;

	@ApiParam(required = true)
	@ApiModelProperty(value = "创建者名称",example = "张三")
	String userName = "";

	@ApiParam(required = true)
	@ApiModelProperty(value = "停服开始时间",example = "2016-12-01 00:00:00")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	Date startTime;

	@ApiParam(required = true)
	@ApiModelProperty(value = "停服结束时间",example = "2016-12-02 11:00:00")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	Date endTime;

	@ApiModelProperty(hidden = true)
	Date createTime = new Date();

	@ApiModelProperty(hidden = true)
	Date modifyTime = new Date();

	@ApiParam(required = false)
	@ApiModelProperty(value = "重定向url，可以为空",example = "https://www.yirendai.com")
	String redirectUrl = "";

	@ApiParam(required = true)
	@ApiModelProperty(value = "需要停服的渠道码（默认为‘*’）,如有多个使用‘|’分隔，如‘dmApp|knApp’、‘*’(代表所有渠道)",example = "*")
	String platformCode = "";

	@ApiParam(required = false)
	@ApiModelProperty(value = "当前配置项状态（是否生效）：0 无效(默认)，1 有效",allowableValues = "0,1",example = "1")
	int status = 0;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@JsonSerialize(using = JsonDateSerializer.class)
	public Date getStartTime() {
		return startTime;
	}

	@JsonDeserialize(using = JsonDateDeserializer.class)
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	@JsonSerialize(using = JsonDateSerializer.class)
	public Date getEndTime() {
		return endTime;
	}

	@JsonDeserialize(using = JsonDateDeserializer.class)
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	@JsonSerialize(using = JsonDateSerializer.class)
	public Date getCreateTime() {
		return createTime;
	}

	@JsonDeserialize(using = JsonDateDeserializer.class)
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@JsonSerialize(using = JsonDateSerializer.class)
	public Date getModifyTime() {
		return modifyTime;
	}

	@JsonDeserialize(using = JsonDateDeserializer.class)
	public void setModifyTime(Date modifyTime) {
		this.modifyTime = modifyTime;
	}

	public String getRedirectUrl() {
		return redirectUrl;
	}

	public void setRedirectUrl(String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}

	public String getPlatformCode() {
		return platformCode;
	}

	public void setPlatformCode(String platformCode) {
		this.platformCode = platformCode;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "SystemStopServiceProfile{" +
				"id=" + id +
				", userName='" + userName + '\'' +
				", startTime=" + startTime +
				", endTime=" + endTime +
				", createTime=" + createTime +
				", modifyTime=" + modifyTime +
				", redirectUrl='" + redirectUrl + '\'' +
				", platformCode='" + platformCode + '\'' +
				", status=" + status +
				'}';
	}
}
