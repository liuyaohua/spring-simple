package com.example.liuyaohua.controller.swagger;

import com.example.liuyaohua.dao.ILogDAO;
import com.example.liuyaohua.model.Log;
import com.example.liuyaohua.model.vo.APIResponse;
import com.example.liuyaohua.util.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>作者 yaohua.liu
 * <p>日期 2017-09-19 20:01
 * <p>说明 日志相关
 */
@Api(description = "日志相关")
@Controller
@RequestMapping("/log")
public class LogController {

	@Autowired
	private ILogDAO mapper;

	@ApiOperation(value = "查询记录(GET)", notes = "查询记录:http method is get", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
	@RequestMapping(value = "/user/queryByGet.json",method = RequestMethod.GET)
	@ResponseBody
	public APIResponse<List<Log>> queryByGet(
			@ApiParam(required = true, hidden = false, value = "用户名") @PathVariable String name,
			@ApiParam(required = true, hidden = false, value = "删除标识",example = "true",allowableValues = "true|false") @PathVariable boolean flag,
			@ApiParam(required = true, value = "当前页",allowableValues = "1,100",example = "5") @RequestParam("currentPage") int currentPage,
			@ApiParam(required = true, value = "每页显示数量") @RequestParam("pageSize") int pageSize) {
		Page page = new Page();
		page.setLow((currentPage - 1) * pageSize);
		page.setHight(currentPage * pageSize);
		page.setUsername(name);
		List<Log> logs = mapper.selectUserLogByPage(page);

		return APIResponse.returnSuccess(logs);
	}

	@ApiOperation(value = "查询记录(POST)", notes = "查询记录:http method is post", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
	@RequestMapping(value = "/user/queryByPost.json",method = RequestMethod.POST)
	@ResponseBody
	public APIResponse<List<Log>> queryByPost(
			@ApiParam(required = true, hidden = false, value = "用户名") @PathVariable String name,
			@ApiParam(required = true, hidden = false, value = "删除标识",example = "true",allowableValues = "true|false") @PathVariable boolean flag,
			@ApiParam(required = true, value = "当前页",allowableValues = "1,5",example = "5") @RequestParam("currentPage") int currentPage,
			@ApiParam(required = true, value = "每页显示数量") @RequestParam("pageSize") int pageSize) {
		Page page = new Page();
		page.setLow((currentPage - 1) * pageSize);
		page.setHight(currentPage * pageSize);
		page.setUsername(name);
		List<Log> logs = mapper.selectUserLogByPage(page);

		return APIResponse.returnSuccess(logs);
	}
}
