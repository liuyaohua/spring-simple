package com.example.liuyaohua.guava.concurrent;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.util.concurrent.AbstractService;
import com.google.common.util.concurrent.MoreExecutors;
import com.google.common.util.concurrent.Service;
import com.google.common.util.concurrent.Service.Listener;
import com.google.common.util.concurrent.Service.State;
import org.junit.Test;

import java.util.List;
import java.util.concurrent.CountDownLatch;

import static java.lang.Thread.currentThread;
import static junit.framework.TestCase.*;

/**
 * 作者 yaohua.liu
 * 日期 2014-11-28 16:16
 * * 说明 ...
 */
public class AbstractServiceTest {

	private static class NoOpService extends AbstractService {
		boolean running = false;

		@Override protected void doStart() {
			assertFalse(running);
			running = true;
			notifyStarted();
		}

		@Override protected void doStop() {
			assertTrue(running);
			running = false;
			notifyStopped();
		}
	}

	@Test
	public void testNoOpServiceStartStop() throws Exception {
		NoOpService service = new NoOpService();
		RecordingListener listener = RecordingListener.record(service);

		assertEquals(State.NEW, service.state());
		assertFalse(service.isRunning());
		assertFalse(service.running);

		service.startAsync();
		assertEquals(State.RUNNING, service.state());
		assertTrue(service.isRunning());
		assertTrue(service.running);

		service.stopAsync();
		assertEquals(State.TERMINATED, service.state());
		assertFalse(service.isRunning());
		assertFalse(service.running);
		assertEquals(
				ImmutableList.of(
						State.STARTING,
						State.RUNNING,
						State.STOPPING,
						State.TERMINATED),
				listener.getStateHistory());
	}

	@Test
	public void testNoOpServiceStartAndWaitStopAndWait() throws Exception {
		NoOpService service = new NoOpService();

		service.startAsync().awaitRunning();
		assertEquals(State.RUNNING, service.state());

		service.stopAsync().awaitTerminated();
		assertEquals(State.TERMINATED, service.state());
	}

	@Test
	public void testNoOpServiceStartAsyncAndAwaitStopAsyncAndAwait() throws Exception {
		NoOpService service = new NoOpService();

		service.startAsync().awaitRunning();
		assertEquals(State.RUNNING, service.state());

		service.stopAsync().awaitTerminated();
		assertEquals(State.TERMINATED, service.state());
	}

	@Test
	public void testNoOpServiceStopIdempotence() throws Exception {
		NoOpService service = new NoOpService();
		RecordingListener listener = RecordingListener.record(service);
		service.startAsync().awaitRunning();
		assertEquals(State.RUNNING, service.state());

		service.stopAsync();
		service.stopAsync();
		assertEquals(State.TERMINATED, service.state());
		assertEquals(
				ImmutableList.of(
						State.STARTING,
						State.RUNNING,
						State.STOPPING,
						State.TERMINATED),
				listener.getStateHistory());
	}

	@Test
	public void testNoOpServiceStopIdempotenceAfterWait() throws Exception {
		NoOpService service = new NoOpService();

		service.startAsync().awaitRunning();

		service.stopAsync().awaitTerminated();
		service.stopAsync();
		assertEquals(State.TERMINATED, service.state());
	}

	@Test
	public void testNoOpServiceStopIdempotenceDoubleWait() throws Exception {
		NoOpService service = new NoOpService();

		service.startAsync().awaitRunning();
		assertEquals(State.RUNNING, service.state());

		service.stopAsync().awaitTerminated();
		service.stopAsync().awaitTerminated();
		assertEquals(State.TERMINATED, service.state());
	}

	@Test
	public void testNoOpServiceStartStopAndWaitUninterruptible()
			throws Exception {
		NoOpService service = new NoOpService();

		currentThread().interrupt();
		try {
			service.startAsync().awaitRunning();
			assertEquals(State.RUNNING, service.state());

			service.stopAsync().awaitTerminated();
			assertEquals(State.TERMINATED, service.state());

			assertTrue(currentThread().isInterrupted());
		} finally {
			Thread.interrupted(); // clear interrupt for future tests
		}
	}

	private static class RecordingListener extends Listener {
		static RecordingListener record(Service service) {
			RecordingListener listener = new RecordingListener(service);
			service.addListener(listener, MoreExecutors.sameThreadExecutor());
			return listener;
		}

		final Service service;

		RecordingListener(Service service) {
			this.service = service;
		}

		final List<State> stateHistory = Lists.newArrayList();
		final CountDownLatch completionLatch = new CountDownLatch(1);

		ImmutableList<State> getStateHistory() throws Exception {
			completionLatch.await();
			synchronized (this) {
				return ImmutableList.copyOf(stateHistory);
			}
		}

		@Override public synchronized void starting() {
			assertTrue(stateHistory.isEmpty());
			assertNotSame(State.NEW, service.state());
			stateHistory.add(State.STARTING);
		}

		@Override public synchronized void running() {
			assertEquals(State.STARTING, Iterables.getOnlyElement(stateHistory));
			stateHistory.add(State.RUNNING);
			service.awaitRunning();
			assertNotSame(State.STARTING, service.state());
		}

		@Override public synchronized void stopping(State from) {
			assertEquals(from, Iterables.getLast(stateHistory));
			stateHistory.add(State.STOPPING);
			if (from == State.STARTING) {
				try {
					service.awaitRunning();
					fail();
				} catch (IllegalStateException expected) {
					assertNull(expected.getCause());
					assertTrue(expected.getMessage().equals(
							"Expected the service to be RUNNING, but was STOPPING"));
				}
			}
			assertNotSame(from, service.state());
		}

		@Override public synchronized void terminated(State from) {
			assertEquals(from, Iterables.getLast(stateHistory, State.NEW));
			stateHistory.add(State.TERMINATED);
			assertEquals(State.TERMINATED, service.state());
			if (from == State.NEW) {
				try {
					service.awaitRunning();
					fail();
				} catch (IllegalStateException expected) {
					assertNull(expected.getCause());
					assertTrue(expected.getMessage().equals(
							"Expected the service to be RUNNING, but was TERMINATED"));
				}
			}
			completionLatch.countDown();
		}

		@Override public synchronized void failed(State from, Throwable failure) {
			assertEquals(from, Iterables.getLast(stateHistory));
			stateHistory.add(State.FAILED);
			assertEquals(State.FAILED, service.state());
			assertEquals(failure, service.failureCause());
			if (from == State.STARTING) {
				try {
					service.awaitRunning();
					fail();
				} catch (IllegalStateException e) {
					assertEquals(failure, e.getCause());
				}
			}
			try {
				service.awaitTerminated();
				fail();
			} catch (IllegalStateException e) {
				assertEquals(failure, e.getCause());
			}
			completionLatch.countDown();
		}
	}
}
