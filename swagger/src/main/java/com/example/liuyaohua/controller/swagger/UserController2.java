package com.example.liuyaohua.controller.swagger;

import com.example.liuyaohua.dao.ILogDAO;
import com.example.liuyaohua.dao.IUserDAO;
import com.example.liuyaohua.model.Log;
import com.example.liuyaohua.model.LogTypeEnum;
import com.example.liuyaohua.model.User;
import com.example.liuyaohua.model.vo.APIResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;

/**
 * <p>作者 yaohua.liu
 * <p>日期 2017-09-19 20:01
 * <p>说明 用户信息
 */
@Api(description = "用户信息2")
@Controller("userController2")
@RequestMapping("/user2")
public class UserController2 {

    @Resource
    private IUserDAO iUserDAO;

    @Resource
    private ILogDAO iLogDAO;

    private Logger logger = LoggerFactory.getLogger(UserController2.class);

    @ApiOperation(value = "查询用户(GET)", notes = "查询用户详细信息", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping(value = "/user/queryByGet.json", params = "name", method = RequestMethod.GET)
    @ResponseBody
    public APIResponse<User> queryByGet(
            @RequestParam(required = true) String name) {
        System.out.println("param name:" + name);
        User user = null;
        try {
            user = iUserDAO.selectUser(name);
            return APIResponse.returnSuccess(user);
        } catch (Exception e) {
            logger.info("查询用户失败", e);
            e.printStackTrace();
            return APIResponse.returnFail("查询用户失败");
        }
    }

    @ApiOperation(value = "添加用户(POST)", notes = "添加用户详细信息", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping(value = "/addByPost", method = RequestMethod.POST)
    @ResponseBody
    public APIResponse<Integer> addByPost(
            @RequestBody User user,
            HttpServletResponse response) {
        try {
            iUserDAO.addUser(user);
            // 记录日志
            Log logger = new Log();
            logger.setUserId(user.getId());
            logger.setType(LogTypeEnum.create_user);
            logger.setContent("create user:" + user.getName());
            logger.setCreatetime(new Date());
            iLogDAO.insertLog(logger);

            return APIResponse.returnSuccess(user.getId());
        } catch (Exception e) {
            e.printStackTrace();
            logger.info("添加用户失败", e);
            return APIResponse.returnFail("添加用户失败");
        }
    }
}
