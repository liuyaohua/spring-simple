package com.example.liuyaohua.guava.io;

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import com.google.common.io.LineProcessor;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * 作者 yaohua.liu
 * 日期 2014-09-05 16:34
 * * 说明 ...
 */
public class FilesTest {

    @Before
    public void setFILE_PATH() {
        String filePath =this.getClass().getClassLoader().getResource("from.txt").getPath();
        System.out.println(filePath);
        String filePath2 =this.getClass().getClassLoader().getResource("").getPath();
        System.out.println(filePath2);
        // FILE_PATH_RESOURCE = FilesTest.class.getResource("/").getFile();
    }

    @Test
    public void testFiles() throws IOException {
        // copy文件 Files.copy
        File from = new File(this.getClass().getClassLoader().getResource("").getPath() + "from.txt");
        File to = new File(this.getClass().getClassLoader().getResource("").getPath() + "to.txt");
        Files.copy(from, to);

        // 把文件转换成String
        String toString= Files.toString(from,Charsets.UTF_8);

        // 写文件 Files.write
        String str = "test";
        to = new File(this.getClass().getClassLoader().getResource("").getPath() + "to.txt");
        Files.write(str.getBytes(), to);

        // 读文件 Files.readLines(File, Charsets.UTF_8)
        List<String> lines = Files.readLines(from, Charsets.UTF_8);

        // 读文件，使用行处理器过滤：LineProcessor
        String result = Files.readLines(from, Charsets.UTF_8, new LineProcessor<String>() {
            StringBuilder stringBuilder = new StringBuilder();

            @Override
            public boolean processLine(String s) throws IOException {
                if (s.contains("System")) {
                    return false;//结束执行
                }
                stringBuilder.append(s + "\n");
                return true;
            }

            @Override
            public String getResult() {// 整个处理器返回结果
                return stringBuilder.toString();
            }
        });

        // 比较文件 Files.equal(file1, file2)
        boolean equal = Files.equal(from, to);

        // 创建一个临时文件夹
        File file = Files.createTempDir();
    }

    /**
     * 其他有用的方法
     * <p/>
     * Guava的Files类中还提供了其他一些文件的简捷方法。比如
     *
     * @param touch                          方法创建或者更新文件的时间戳。
     * @param createTempDir()                方法创建临时目录
     * @param Files.createParentDirs(File)   创建父级目录
     * @param getChecksum(File)              获得文件的checksum
     * @param hash(File)                     获得文件的hash
     * @param map                            系列方法获得文件的内存映射
     * @param getFileExtension(String)       获得文件的扩展名
     * @param getNameWithoutExtension(String file) 获得不带扩展名的文件名
     */
    public void test() {
    }
}
