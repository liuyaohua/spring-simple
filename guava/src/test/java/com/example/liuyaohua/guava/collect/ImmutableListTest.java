package com.example.liuyaohua.guava.collect;

import com.google.common.collect.ImmutableList;
import org.junit.Test;

import java.util.Iterator;

/**
 * 作者 yaohua.liu
 * 日期 2014-11-24 17:24
 * * 说明
 * 为什么要使用不可变集合？
 * 1）当对象被不可信的库调用时，不可变形式是安全的；
 * 2）不可变对象被多个线程调用时，不存在竞态条件问题
 * 3）不可变集合不需要考虑变化，因此可以节省时间和空间。所有不可变的集合都比它们的可变形式有更好的内存利用率（分析和测试细节）
 * 4）不可变对象因为有固定不变，可以作为常量来安全使用。
 * 5）创建对象的不可变拷贝是一项很好的防御性编程技巧。Guava为所有JDK标准集合类型和Guava新集合类型都提供了简单易用的不可变版本。
 *  重要提示：所有Guava不可变集合的实现都不接受null值。如果你需要在不可变集合中使用null，请使用JDK中的Collections.unmodifiableXXX方法
 */
public class ImmutableListTest {
    @Test
    public void testImmutableList() {
        ImmutableList<String> list = ImmutableList.of("1", "3", "2");

        // 子列表
        ImmutableList<String> subList = list.subList(1, 2);
        System.out.println(subList);

        // 反转列表
        ImmutableList<String> reverse = list.reverse();
        System.out.println(reverse);

        //迭代器
        Iterator<String> iterator = list.iterator();
    }
}
