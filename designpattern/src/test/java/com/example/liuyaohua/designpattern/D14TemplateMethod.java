package com.example.liuyaohua.designpattern;

/**
 * 作者 yaohua.liu
 * 日期 2017-06-01 16:47
 * 说明 模板方法模式（Template Method）
 * 模板方法模式可以在一个方法中定义一个算法的骨架，而将具体的实现步骤延迟到子类中去实现。这样，可以在不改变算法结构的基础上，重新定义算法的步骤。
 */
public class D14TemplateMethod {

	public static void main(String[] args) {
		String exp = "8+8";
		AbstractCalculator2 cal = new Plus2();
		int result = cal.calculate(exp, "\\+");
		System.out.println(result);
	}
}

abstract class AbstractCalculator2 {

	/*主方法，实现对本类其它方法的调用*/
	public final int calculate(String exp, String opt) {
		int array[] = split(exp, opt);
		return calculate(array[0], array[1]);
	}

	/*被子类重写的方法*/
	abstract public int calculate(int num1, int num2);

	public int[] split(String exp, String opt) {
		String array[] = exp.split(opt);
		int arrayInt[] = new int[2];
		arrayInt[0] = Integer.parseInt(array[0]);
		arrayInt[1] = Integer.parseInt(array[1]);
		return arrayInt;
	}
}

class Plus2 extends AbstractCalculator2 {

	@Override
	public int calculate(int num1, int num2) {
		return num1 + num2;
	}
}