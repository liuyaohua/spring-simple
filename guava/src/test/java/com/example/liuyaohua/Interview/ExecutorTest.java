package com.example.liuyaohua.Interview;

import org.junit.Test;

import java.util.concurrent.*;

/**
 * 作者 yaohua.liu
 * 日期 2017-05-09 17:04
 * 说明 ...
 */
public class ExecutorTest {

	@Test
	public void test() throws InterruptedException, ExecutionException, TimeoutException {
		ExecutorService executor = Executors.newFixedThreadPool(15);

		Future<java.lang.String> future = executor.submit(new Callable<java.lang.String>() {
			public java.lang.String call() {
				return "";
			}
		});

		try {
			future.get(10, TimeUnit.HOURS); // use future
		} catch (ExecutionException ex) {
			return;
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (TimeoutException e) {
			e.printStackTrace();
		}

		FutureTask<java.lang.String> task = new FutureTask<java.lang.String>(new Callable<java.lang.String>() {
			public java.lang.String call() {
				return "";
			}
		});
		executor.execute(task);
		task.get(1000, TimeUnit.MICROSECONDS);

		Thread thread = new Thread(new Runnable() {
			@Override
			public void run() {

			}
		});
		thread.start();
//		thread.wait();
	}
}
