package com.example.liuyaohua.guava.concurrent;

import com.google.common.util.concurrent.AbstractScheduledService;
import com.google.common.util.concurrent.MoreExecutors;
import com.google.common.util.concurrent.Service;
import org.junit.Test;

import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.Assert.*;

/**
 * 作者 yaohua.liu
 * 日期 2014-11-28 15:26
 * * 说明 ...
 */
public class AbstractScheduledServiceTest {

    volatile AbstractScheduledService.Scheduler configuration = AbstractScheduledService.Scheduler.newFixedDelaySchedule(0, 10, TimeUnit.MILLISECONDS);
    volatile ScheduledFuture<?> future = null;

    volatile boolean atFixedRateCalled = false;
    volatile boolean withFixedDelayCalled = false;
    volatile boolean scheduleCalled = false;

    final ScheduledExecutorService executor = new ScheduledThreadPoolExecutor(10) {
        @Override
        public ScheduledFuture<?> scheduleWithFixedDelay(Runnable command, long initialDelay,
                long delay, TimeUnit unit) {
            return future = super.scheduleWithFixedDelay(command, initialDelay, delay, unit);
        }
    };

    @Test
    public void testServiceStartStop() throws Exception {
        NullService service = new NullService();
        service.startAsync().awaitRunning();
        assertFalse(future.isDone());
        service.stopAsync().awaitTerminated();
        assertTrue(future.isCancelled());
    }

    private class NullService extends AbstractScheduledService {
        @Override protected void runOneIteration() throws Exception {
        }

        @Override protected Scheduler scheduler() {
            return configuration;
        }

        @Override protected ScheduledExecutorService executor() {
            return executor;
        }
    }

    @Test(expected = java.util.concurrent.CancellationException.class)
    public void testFailOnExceptionFromRun() throws Exception {
        TestService service = new TestService();
        service.runException = new Exception();
        service.startAsync().awaitRunning();
        service.runFirstBarrier.await();
        service.runSecondBarrier.await();
        try {
            future.get();
            fail();
        } catch (ExecutionException e) {
            // An execution exception holds a runtime exception (from throwables.propogate) that holds our
            // original exception.
            assertEquals(service.runException, e.getCause().getCause());
        }
        assertEquals(service.state(), Service.State.FAILED);
    }

    public void testFailOnExceptionFromStartUp() {
        TestService service = new TestService();
        service.startUpException = new Exception();
        try {
            service.startAsync().awaitRunning();
            fail();
        } catch (IllegalStateException e) {
            assertEquals(service.startUpException, e.getCause());
        }
        assertEquals(0, service.numberOfTimesRunCalled.get());
        assertEquals(Service.State.FAILED, service.state());
    }

    public void testFailOnExceptionFromShutDown() throws Exception {
        TestService service = new TestService();
        service.shutDownException = new Exception();
        service.startAsync().awaitRunning();
        service.runFirstBarrier.await();
        service.stopAsync();
        service.runSecondBarrier.await();
        try {
            service.awaitTerminated();
            fail();
        } catch (IllegalStateException e) {
            assertEquals(service.shutDownException, e.getCause());
        }
        assertEquals(Service.State.FAILED, service.state());
    }

    public void testRunOneIterationCalledMultipleTimes() throws Exception {
        TestService service = new TestService();
        service.startAsync().awaitRunning();
        for (int i = 1; i < 10; i++) {
            service.runFirstBarrier.await();
            assertEquals(i, service.numberOfTimesRunCalled.get());
            service.runSecondBarrier.await();
        }
        service.runFirstBarrier.await();
        service.stopAsync();
        service.runSecondBarrier.await();
        service.stopAsync().awaitTerminated();
    }

    public void testExecutorOnlyCalledOnce() throws Exception {
        TestService service = new TestService();
        service.startAsync().awaitRunning();
        // It should be called once during startup.
        assertEquals(1, service.numberOfTimesExecutorCalled.get());
        for (int i = 1; i < 10; i++) {
            service.runFirstBarrier.await();
            assertEquals(i, service.numberOfTimesRunCalled.get());
            service.runSecondBarrier.await();
        }
        service.runFirstBarrier.await();
        service.stopAsync();
        service.runSecondBarrier.await();
        service.stopAsync().awaitTerminated();
        // Only called once overall.
        assertEquals(1, service.numberOfTimesExecutorCalled.get());
    }

    public void testDefaultExecutorIsShutdownWhenServiceIsStopped() throws Exception {
        final CountDownLatch terminationLatch = new CountDownLatch(1);
        AbstractScheduledService service = new AbstractScheduledService() {
            volatile ScheduledExecutorService executorService;

            @Override protected void runOneIteration() throws Exception {
            }

            @Override protected ScheduledExecutorService executor() {
                if (executorService == null) {
                    executorService = super.executor();
                    // Add a listener that will be executed after the listener that shuts down the executor.
                    addListener(new Listener() {
                        @Override public void terminated(State from) {
                            terminationLatch.countDown();
                        }
                    }, MoreExecutors.sameThreadExecutor());
                }
                return executorService;
            }

            @Override protected Scheduler scheduler() {
                return Scheduler.newFixedDelaySchedule(0, 1, TimeUnit.MILLISECONDS);
            }
        };

        service.startAsync();
        //assertFalse(service.executor().isShutdown());
        service.awaitRunning();
        service.stopAsync();
        terminationLatch.await();
        //assertTrue(service.executor().isShutdown());
        //assertTrue(service.executor().awaitTermination(100, TimeUnit.MILLISECONDS));
    }

    public void testDefaultExecutorIsShutdownWhenServiceFails() throws Exception {
        final CountDownLatch failureLatch = new CountDownLatch(1);
        AbstractScheduledService service = new AbstractScheduledService() {
            volatile ScheduledExecutorService executorService;

            @Override protected void runOneIteration() throws Exception {
            }

            @Override protected void startUp() throws Exception {
                throw new Exception("Failed");
            }

            @Override protected ScheduledExecutorService executor() {
                if (executorService == null) {
                    executorService = super.executor();
                    // Add a listener that will be executed after the listener that shuts down the executor.
                    addListener(new Listener() {
                        @Override public void failed(State from, Throwable failure) {
                            failureLatch.countDown();
                        }
                    }, MoreExecutors.sameThreadExecutor());
                }
                return executorService;
            }

            @Override protected Scheduler scheduler() {
                return Scheduler.newFixedDelaySchedule(0, 1, TimeUnit.MILLISECONDS);
            }
        };

        try {
            service.startAsync().awaitRunning();
            fail("Expected service to fail during startup");
        } catch (IllegalStateException expected) {
        }
        failureLatch.await();
        //assertTrue(service.executor().isShutdown());
        //assertTrue(service.executor().awaitTermination(100, TimeUnit.MILLISECONDS));
    }

    public void testSchedulerOnlyCalledOnce() throws Exception {
        TestService service = new TestService();
        service.startAsync().awaitRunning();
        // It should be called once during startup.
        assertEquals(1, service.numberOfTimesSchedulerCalled.get());
        for (int i = 1; i < 10; i++) {
            service.runFirstBarrier.await();
            assertEquals(i, service.numberOfTimesRunCalled.get());
            service.runSecondBarrier.await();
        }
        service.runFirstBarrier.await();
        service.stopAsync();
        service.runSecondBarrier.await();
        service.awaitTerminated();
        // Only called once overall.
        assertEquals(1, service.numberOfTimesSchedulerCalled.get());
    }

    private class TestService extends AbstractScheduledService {
        CyclicBarrier runFirstBarrier = new CyclicBarrier(2);
        CyclicBarrier runSecondBarrier = new CyclicBarrier(2);

        volatile boolean startUpCalled = false;
        volatile boolean shutDownCalled = false;
        AtomicInteger numberOfTimesRunCalled = new AtomicInteger(0);
        AtomicInteger numberOfTimesExecutorCalled = new AtomicInteger(0);
        AtomicInteger numberOfTimesSchedulerCalled = new AtomicInteger(0);
        volatile Exception runException = null;
        volatile Exception startUpException = null;
        volatile Exception shutDownException = null;

        @Override
        protected void runOneIteration() throws Exception {
            assertTrue(startUpCalled);
            assertFalse(shutDownCalled);
            numberOfTimesRunCalled.incrementAndGet();
            assertEquals(State.RUNNING, state());
            runFirstBarrier.await();
            runSecondBarrier.await();
            if (runException != null) {
                throw runException;
            }
        }

        @Override
        protected void startUp() throws Exception {
            assertFalse(startUpCalled);
            assertFalse(shutDownCalled);
            startUpCalled = true;
            assertEquals(State.STARTING, state());
            if (startUpException != null) {
                throw startUpException;
            }
        }

        @Override
        protected void shutDown() throws Exception {
            assertTrue(startUpCalled);
            assertFalse(shutDownCalled);
            shutDownCalled = true;
            if (shutDownException != null) {
                throw shutDownException;
            }
        }

        @Override
        protected ScheduledExecutorService executor() {
            numberOfTimesExecutorCalled.incrementAndGet();
            return executor;
        }

        @Override
        protected Scheduler scheduler() {
            numberOfTimesSchedulerCalled.incrementAndGet();
            return configuration;
        }
    }
}
