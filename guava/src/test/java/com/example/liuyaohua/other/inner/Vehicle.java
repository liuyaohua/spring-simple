package com.example.liuyaohua.other.inner;

/**
 * 作者 yaohua.liu
 * 日期 2015-09-07 19:07
 *
 * 说明 ...
 */
public interface Vehicle {
    void drive();
}
