package com.example.liuyaohua.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;

@ApiModel(value = "data", description = "基本信息")
public class Log  implements Serializable {
	@ApiModelProperty(value = "id",required = true,example = "1000")
	@JsonIgnore
	private int id;
    @ApiModelProperty(value = "用户id",required = true,example = "1000")
	private int userId;
    @ApiModelProperty(value = "type",required = true,example = "1",allowableValues = "0,1")
	private LogTypeEnum type;
    @ApiModelProperty(value = "联系方式",required = true,example = "17090078480")
	private String content;
    @ApiModelProperty(value = "创建时间",required = true,hidden = true, example = "微秒")
	private Date createtime;

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}


	public String getContent() {
		return content;
	}

	public LogTypeEnum getType() {
		return type;
	}

	public void setType(LogTypeEnum type) {
		this.type = type;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	@JsonSerialize(using=JsonDateSerializer.class)
	public Date getCreatetime() {
		return createtime;
	}

	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}
}
