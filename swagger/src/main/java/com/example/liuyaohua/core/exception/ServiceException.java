package com.example.liuyaohua.core.exception;

import com.example.liuyaohua.model.enums.ErrorCode;
import com.example.liuyaohua.model.enums.ErrorCode;

/**
 * 作者 yaohua.liu
 * 日期 2016-08-01 11:48
 * 说明 业务异常
 */
public class ServiceException extends Exception {

	private static final long serialVersionUID = -2196159509771060128L;
	
	private String code;
	/**
	 * 是否打印栈信息，默认打印
	 */
	private boolean printStackInfo = true;

	public ServiceException() {
		super();
	}

	/**
	 *
	 * @param code 错误码
	 * @param message 错误消息
     */
	public ServiceException(final String code,final String message) {
		super(message);
		this.code = code;
	}

	/**
	 * 异常
	 * @param cause
     */
	public ServiceException(final Throwable cause) {
		super(cause);
	}

	public ServiceException(final ErrorCode errorCode) {
		super(errorCode.getMessage());
		this.code = errorCode.getCode();
	}

	/**
	 *
	 * @param errorCode ErrorCode
	 * @param printStackInfo 是否打印栈信息，true 打印, false 不打印
	 */
	public ServiceException(final ErrorCode errorCode, final boolean printStackInfo) {
		super(errorCode.getMessage());
		this.code = errorCode.getCode();
		this.printStackInfo = printStackInfo;
	}

	public ServiceException(final ErrorCode errorCode, final String message) {
		super(message);
		if (errorCode != null) {
			this.code = errorCode.getCode();
		}
	}

	public ServiceException(final ErrorCode errorCode, final String message, final boolean printStackInfo) {
		super(message);
		if (errorCode != null) {
			this.code = errorCode.getCode();
		}
		this.printStackInfo = printStackInfo;
	}

	public ServiceException(final String code, final String message, final boolean printStackInfo) {
		super(message);
		this.code = code;
		this.printStackInfo = printStackInfo;
	}

	public ServiceException(final ErrorCode errorCode, final String message, final Throwable cause) {
		super(message, cause);
		if (errorCode != null) {
			this.code = errorCode.getCode();
		}
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public boolean isPrintStackInfo() {
		return printStackInfo;
	}
}
