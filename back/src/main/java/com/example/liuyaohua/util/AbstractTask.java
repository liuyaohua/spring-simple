package com.example.liuyaohua.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractTask {

	protected final Logger logger = LoggerFactory.getLogger(getClass());

	/**
	 * 任务名称
	 * 
	 * @return
	 */
	public abstract String taskName();

	/**
	 * 任务执行逻辑
	 */
	protected abstract void run() ;

	/**
	 * 任务调度入口
	 */
	public void doSchedule() {
		long start = System.currentTimeMillis();
		try {
			logger.info(this.taskName() + "[" + getClass().getCanonicalName() + "]开始执行...............................");
			this.run();
			logger.info(this.taskName() + "[" + getClass().getCanonicalName() + "]执行完毕,耗时[" + (System.currentTimeMillis() - start) + "]ms");
		} catch (Exception e) {
			logger.error("定时任务[" + this.taskName() + ":" + getClass().getCanonicalName() + "]运行出错", e);
		}
	}
}
