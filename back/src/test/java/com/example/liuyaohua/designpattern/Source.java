package com.example.liuyaohua.designpattern;

/**
 * 作者 yaohua.liu
 * 日期 2017-06-01 15:31
 * 说明 ...
 */
class Source implements Sourceable {

	@Override
	public void method() {
		System.out.println("the original method!");
	}
}
