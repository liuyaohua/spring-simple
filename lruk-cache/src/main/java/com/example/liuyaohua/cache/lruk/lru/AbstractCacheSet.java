package com.example.liuyaohua.cache.lruk.lru;

import java.util.AbstractSet;
import java.util.concurrent.ConcurrentMap;

/**
 * 作者 yaohua.liu
 * 日期 2018-03-24 12:45
 * 说明 ...
 */
public abstract class AbstractCacheSet<T> extends AbstractSet<T> {
    protected final ConcurrentMap<?, ?> map;

    protected AbstractCacheSet(ConcurrentMap<?, ?> map) {
        this.map = map;
    }

    @Override
    public int size() {
        return map.size();
    }

    @Override
    public boolean isEmpty() {
        return map.isEmpty();
    }

    @Override
    public void clear() {
        map.clear();
    }

    // super.toArray() may misbehave if size() is inaccurate, at least on old versions of Android.
    // https://code.google.com/p/android/issues/detail?id=36519 / http://r.android.com/47508

    @Override
    public Object[] toArray() {
        return LocalCache.toArrayList(this).toArray();
    }

    @Override
    public <E> E[] toArray(E[] a) {
        return LocalCache.toArrayList(this).toArray(a);
    }
}
