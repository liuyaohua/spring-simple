package com.example.liuyaohua;

import com.example.liuyaohua.model.User;
import com.example.liuyaohua.service.UserService;
import org.junit.Assert;
import org.junit.Test;

import javax.annotation.Resource;
import java.util.Random;

/**
 * <p>作者 yaohua.liu
 * <p>日期 2018-09-02 20:37
 * <p>说明
 */
//继承单元测试基类CommonTest.java
public class UserServiceTest extends CommonTest {

    @Resource //注入Service使用@Resource或@Autowired
    private UserService userService;

    /**
     * 创建测试模型
     */
    private User createUser() {
        Random random = new Random(1000000);
        User user = new User();
        user.setStatus(0);
        user.setName("new name_" + random.nextInt());
        return user;
    }

    @Test
    public void saveUser() {
        User user = createUser();
        // 保存数据
        userService.saveUser(user);
        Assert.assertTrue(user.getId() > 0);

        // 校验保存的数据与查询出来的数据是否一致
        User temp = userService.queryUserById(user.getId());
        Assert.assertNotNull(temp);
        Assert.assertNotNull(temp.getName());
        Assert.assertEquals(user.getName(), temp.getName());
        Assert.assertEquals(user.getStatus(), temp.getStatus());
    }

    @Test
    public void queryUserById() {
        // 检查初始化脚本加载数据是否正确
        User user = userService.queryUserById(100);
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getName());
        Assert.assertEquals("test1", user.getName());
    }

    @Test
    public void updateUserById() {
        User user = createUser();
        // 保存数据
        userService.saveUser(user);
        int id = user.getId();
        Assert.assertTrue(id > 0);

        // 校验保存的数据与查询出来的数据是否一致
        User temp = userService.queryUserById(id);
        Assert.assertNotNull(temp);
        Assert.assertNotNull(temp.getName());
        Assert.assertEquals(user.getName(), temp.getName());
        Assert.assertEquals(user.getStatus(), temp.getStatus());
        // 修改信息+更新数据
        user.setStatus(1000);
        user.setName("new-name" + new Random(1000000).nextInt());
        int updateResult = userService.updateUserById(user);
        Assert.assertEquals(1, updateResult);

        // 校验更新数据是否成功
        temp = userService.queryUserById(id);
        Assert.assertNotNull(temp);
        Assert.assertNotNull(temp.getName());
        Assert.assertEquals(user.getName(), temp.getName());
        Assert.assertEquals(user.getStatus(), temp.getStatus());
    }

    @Test
    public void deleteUserById() {
        // 检验数据库是否存在数据
        User user = userService.queryUserById(100);
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getName());
        Assert.assertEquals("test1", user.getName());

        // 校验删除
        int result = userService.deleteUserById(100);
        Assert.assertEquals(1, result);
    }
}