package com.example.liuyaohua.util;

import com.example.liuyaohua.model.enums.ErrorCode;
import com.example.liuyaohua.model.enums.ErrorCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * 作者 yaohua.liu
 * 日期 2016-09-12 11:43
 * 说明 返回信息
 */
@ApiModel(value = "返回信息包装类", description = "ResponseVO")
public class ResponseUtil<T> implements Serializable {

	private static final long serialVersionUID = -6569452449017308033L;

	public static final int SUCCESS = 0;

	public static final int ERROR = 1;

	/**
	 * 0:正常;1:错误;-1:异常
	 */
	@ApiModelProperty(value = "状态码，0：成功，其它：失败/错误/异常", allowableValues = "0|1",example = "0", position = 0)
	private int status;

	/**
	 * 系统错误码
	 */
	@ApiModelProperty(value = "错误码（仅当status!=0时存在）",position = 1)
	private String errorcode;

	/**
	 * 系统错误信息
	 */
	@ApiModelProperty(value = "错误信息（仅当status!=0时存在）", position = 2)
	private String errormsg;

	/**
	 * 业务数据
	 */
	@ApiModelProperty(name = "data", notes = "业务数据（不一定存在）", position = 5)
	private T data;

	public ResponseUtil() {
	}

	public ResponseUtil(int status) {
		this.status = status;
	}

	public ResponseUtil(int status, T data) {
		this.status = status;
		this.data = data;
	}

	public ResponseUtil(int status, String errorcode, String errormsg) {
		this(status, errorcode, errormsg, null);
	}

	public ResponseUtil(int status, String errorcode, String errormsg, T data) {
		this.status = status;
		this.errorcode = errorcode;
		this.errormsg = errormsg;
		this.data = data;
	}


	/**
	 * 构建 成功 返回对象
	 *
	 * @return
	 */
	public static <T> ResponseUtil<T> returnSuccess() {
		return returnSuccess(null);
	}

	/**
	 * 构建 成功 返回对象
	 *
	 * @param t 返回数据
	 * @return
	 */
	public static <T> ResponseUtil<T> returnSuccess(T t) {
		return new ResponseUtil<T>(SUCCESS, t);
	}

	/**
	 * 构建 失败 返回对象
	 *
	 * @param errorCode 错误信息
	 * @return
	 */
	public static <T> ResponseUtil<T> returnFail(ErrorCode errorCode) {
		return returnFail(errorCode.getCode(), errorCode.getMessage());
	}

	/**
	 * 构建 失败 返回对象
	 *
	 * @param errorCode 错误信息
	 * @param <T>       数据
	 * @return
	 */
	public static <T> ResponseUtil<T> returnFail(ErrorCode errorCode, T t) {
		return returnFail(errorCode.getCode(), errorCode.getMessage(), t);
	}

	/**
	 * 构建 失败 返回对象
	 *
	 * @param errorCode 错误码
	 * @param errorMsg  错误信息
	 * @return
	 */
	public static <T> ResponseUtil<T> returnFail(String errorCode, String errorMsg) {
		return returnFail(errorCode, errorMsg, null);
	}

	/**
	 * 构建 失败 返回对象
	 *
	 * @param errorCode 错误码
	 * @param errorMsg  错误信息
	 * @param <T>       数据
	 * @return
	 */
	public static <T> ResponseUtil<T> returnFail(String errorCode, String errorMsg, T t) {
		return new ResponseUtil<T>(ERROR, errorCode, errorMsg, t);
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getErrorcode() {
		return errorcode;
	}

	public void setErrorcode(String errorcode) {
		this.errorcode = errorcode;
	}

	public String getErrormsg() {
		return errormsg;
	}

	public void setErrormsg(String errormsg) {
		this.errormsg = errormsg;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}
}
