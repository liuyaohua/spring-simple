package com.example.liuyaohua.core.annotation;

import java.lang.annotation.*;

/**
 * 作者 yaohua.liu
 * 日期 2016-12-20 16:31
 * 说明 用于注解于token
 */
@Target({ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface BaseInfoType {
}
