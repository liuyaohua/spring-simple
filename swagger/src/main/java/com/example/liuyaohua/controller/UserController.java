package com.example.liuyaohua.controller;

import com.example.liuyaohua.model.Log;
import com.example.liuyaohua.model.LogTypeEnum;
import com.example.liuyaohua.model.User;
import com.example.liuyaohua.model.UserStatusEnum;
import com.google.common.base.Objects;
import com.google.common.collect.Maps;
import com.example.liuyaohua.dao.ILogDAO;
import com.example.liuyaohua.dao.IUserDAO;
import com.example.liuyaohua.model.Log;
import com.example.liuyaohua.model.LogTypeEnum;
import com.example.liuyaohua.model.User;
import com.example.liuyaohua.model.UserStatusEnum;
import com.example.liuyaohua.model.vo.APIResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * <p>作者 yaohua.liu
 * <p>日期 2017-09-19 20:01
 * <p>说明 用户信息
 */
@Api(description = "用户信息")
@Controller
@RequestMapping("/user")
public class UserController {

	@Resource
	private IUserDAO iUserDAO;

	@Resource
	private ILogDAO iLogDAO;

	private Logger logger = LoggerFactory.getLogger(UserController.class);

	@ApiOperation(value = "查询用户", notes = "查询用户", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
	@RequestMapping(value = "/user.json", params = "name", method = RequestMethod.GET)
	@ResponseBody
	public APIResponse<User> selectUser(
			@ApiParam(required = true, value = "用户名") @RequestParam(required = true) String name) {
		System.out.println("param name:" + name);
		User user = null;
		try {
			user = iUserDAO.selectUser(name);
			return APIResponse.returnSuccess(user);
		} catch (Exception e) {
			logger.info("查询用户失败", e);
			e.printStackTrace();
			return APIResponse.returnFail("查询用户失败");
		}

	}

	@ApiOperation(value = "添加用户", notes = "添加用户", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	@ResponseBody
	public APIResponse<Integer> addUser(
			@RequestBody User user,
			HttpServletResponse response) {

		try {

			iUserDAO.addUser(user);
			// 记录日志
			Log logger = new Log();
			logger.setUserId(user.getId());
			logger.setType(LogTypeEnum.create_user);
			logger.setContent("create user:" + user.getName());
			logger.setCreatetime(new Date());
			iLogDAO.insertLog(logger);

			return APIResponse.returnSuccess(user.getId());
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("添加用户失败", e);
			return APIResponse.returnFail("添加用户失败");
		}
	}

	@RequestMapping(value = "/update.json", params = "name", method = RequestMethod.POST)
	@ResponseBody
	public APIResponse<User> updateUser(@RequestBody User user, @RequestParam String name) {
		Map<String, Object> map = Maps.newLinkedHashMap();
		user.setName(name);
		User u1 = null;
		User oldUser = null;
		StringBuilder content = null;
		try {
			oldUser = iUserDAO.selectUser(name);
			Date d1 = oldUser.getBirth();
			iUserDAO.updateUser(user);
			u1 = iUserDAO.selectUser(name);
			Date d2 = u1.getBirth();
			if (!(d1 == d2)) {
				Date birthDate = user.getBirth();
				content = new StringBuilder();
				content.append("[birth]"
						+ new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
						.format(oldUser.getBirth())
						+ "=>"
						+ new SimpleDateFormat("yyyy-MM-dd HH:ss")
						.format(birthDate));
			}

			if (!Objects.equal(user.getStatus(), u1.getStatus())) {
				UserStatusEnum statusEnum = user.getStatus();
				content.append(content + "[status]" + oldUser.getStatus() + "=>" + statusEnum);
			}

		} catch (Exception e) {
			e.printStackTrace();
			logger.info("更新用户失败", e);
		}
		Log logger = new Log();
		logger.setUserId(u1.getId());
		logger.setType(LogTypeEnum.update_user);
		logger.setContent(content + "");
		logger.setCreatetime(new Date());
		iLogDAO.insertLog(logger);
		return APIResponse.returnSuccess(u1);
	}

}
